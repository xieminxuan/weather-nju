# SMART-SPS环境需求



## NCL

美国国家大气研究中心命令语言（**N**CAR **C**ommand **L**anguage）是由美国国家大气研究中心（NCAR）研发的专门用于科学数据处理和可视化的一门开源解释性语言，其官方网页为 http://www.ncl.ucar.edu/。

安装详见 [此处](http://www.ncl.ucar.edu/Download/)，推荐使用conda安装：

`conda install -c conda-forge ncl`



## CDO

CDO （**C**limate **D**ata **O**perators） 是一系列用来处理和分析科学数据的命令行指令合集。官网为 https://code.mpimet.mpg.de/projects/cdo

安装详见[此处](https://code.mpimet.mpg.de/projects/cdo/wiki/Cdo#Installation-and-Supported-Platforms)，推荐使用conda安装：

`conda install -c conda-forge cdo`

