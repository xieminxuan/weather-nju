-- MySQL dump 10.13  Distrib 5.7.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: weather
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `basetasktemplate`
--

DROP TABLE IF EXISTS `basetasktemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `basetasktemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `script` varchar(40) NOT NULL,
  `format` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `basetasktemplate_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `basetasktemplate`
--

LOCK TABLES `basetasktemplate` WRITE;
/*!40000 ALTER TABLE `basetasktemplate` DISABLE KEYS */;
INSERT INTO `basetasktemplate` VALUES (1,'全国预测','预测全国范围内的气温和降水平均距','run.app.smart.main','/figures/rain160x.predict.{fyear}.{fcst}.{fcstseason}.{baseseason}.{varname}.{nopt_modes_olr}.{nopt_modes_hgt}.png');
/*!40000 ALTER TABLE `basetasktemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `childrentaskinstance`
--

DROP TABLE IF EXISTS `childrentaskinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `childrentaskinstance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `parentId` bigint(20) NOT NULL,
  `childrentemplateId` bigint(20) NOT NULL,
  `input` longtext,
  `state` int(11) NOT NULL DEFAULT '0',
  `output` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `childrentaskinstance_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `childrentaskinstance`
--

LOCK TABLES `childrentaskinstance` WRITE;
/*!40000 ALTER TABLE `childrentaskinstance` DISABLE KEYS */;
INSERT INTO `childrentaskinstance` VALUES (1,2,1,1,'2020 forecast{预报} MJJ{5-7月(MJJ)} DJF{3月} temp{气温} 3 3',0,'');
/*!40000 ALTER TABLE `childrentaskinstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `childrentasktemplate`
--

DROP TABLE IF EXISTS `childrentasktemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `childrentasktemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentId` bigint(20) NOT NULL,
  `baseId` bigint(20) NOT NULL,
  `index` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `childrentasktemplate_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `childrentasktemplate`
--

LOCK TABLES `childrentasktemplate` WRITE;
/*!40000 ALTER TABLE `childrentasktemplate` DISABLE KEYS */;
INSERT INTO `childrentasktemplate` VALUES (1,1,1,1);
/*!40000 ALTER TABLE `childrentasktemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `params`
--

DROP TABLE IF EXISTS `params`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `params` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `baseId` bigint(20) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `index` int(11) NOT NULL,
  `cname` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `params_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `params`
--

LOCK TABLES `params` WRITE;
/*!40000 ALTER TABLE `params` DISABLE KEYS */;
INSERT INTO `params` VALUES (1,1,'list','2020 2019','fyear',1,'预测年份'),(2,1,'select','forecast{预报}','fcst',2,'预测类型'),(3,1,'select','JJA{6-8月(JJA夏季)} MJJ{5-7月(MJJ)}','fcstseason',3,'预测季节'),(4,1,'select','DJF{3月} JFM{4月}','baseseason',4,'起报时间'),(5,1,'select','rainfall{降水距平百分率} temp{气温}','varname',5,'变量'),(6,1,'list','1 2 3 4 5 6 7 8 9 10','nopt_modes_olr',6,'OLR模态数'),(7,1,'list','1 2 3 4 5 6 7 8 9 10','nopt_modes_hgt',7,'HGT模态数');
/*!40000 ALTER TABLE `params` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taskinstance`
--

DROP TABLE IF EXISTS `taskinstance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taskinstance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `templateId` bigint(20) NOT NULL,
  `state` int(11) NOT NULL,
  `time` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskinstance_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taskinstance`
--

LOCK TABLES `taskinstance` WRITE;
/*!40000 ALTER TABLE `taskinstance` DISABLE KEYS */;
INSERT INTO `taskinstance` VALUES (1,2,'全国预测气温',1,1,'2020/11/12');
/*!40000 ALTER TABLE `taskinstance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasktemplate`
--

DROP TABLE IF EXISTS `tasktemplate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasktemplate` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tasktemplate_name_uindex` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasktemplate`
--

LOCK TABLES `tasktemplate` WRITE;
/*!40000 ALTER TABLE `tasktemplate` DISABLE KEYS */;
INSERT INTO `tasktemplate` VALUES (1,'全国预测','预测全国范围内的气温和降水平均距');
/*!40000 ALTER TABLE `tasktemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `role` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','123456',1,2),(2,'user','123456',1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-07 11:05:51
