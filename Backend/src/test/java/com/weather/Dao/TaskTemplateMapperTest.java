package com.weather.Dao;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TaskTemplateMapperTest {

    @Test
    void getTaskTemplates() {
    }

    @Test
    void getChildrenTaskTemplates() {
    }

    @Test
    void getBaseTaskTemplateById() {
    }

    @Test
    void getParamsByBaseId() {
    }

    @Test
    void getTaskTemplateById() {
    }

    @Test
    void getChildrenTaskTemplateById() {
    }

    @Test
    void deleteTaskTemplate() {
    }

    @Test
    void deleteChildrenTemplates() {
    }

    @Test
    void getBaseTaskTemplates() {
    }

    @Test
    void addTaskTemplate() {
    }

    @Test
    void getTaskTemplateByName() {
    }

    @Test
    void addChildrenTaskTemplate() {
    }

    @Test
    void updateTaskTemplate() {
    }
}