package com.weather.Dao;

import com.weather.PO.ChildrenTaskInstance;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class TaskMapperTest {
    @Autowired
    TaskMapper taskMapper;

    @Test
    void getTaskInsatances() {
    }

    @Test
    void getTaskInstanceByUserId() {
    }

    @Test
    void addTaskInstance() {
    }

    @Test
    void addChildrenTaskInstance() {
    }

    @Test
    void getTaskInstanceById() {
    }

    @Test
    void getChildrenTaskInstancesByParentId() {
        long parentId=14;
        List<ChildrenTaskInstance> childrenTaskInstanceList=taskMapper.getChildrenTaskInstancesByParentId(parentId);
        assertEquals(3,childrenTaskInstanceList.size());
    }

    @Test
    void getAllTasks() {
    }

    @Test
    void getAllTasksOrderedByUser() {
    }

    @Test
    void getAllTasksOrderedByTime() {
    }

    @Test
    void getAllTasksOrderedByTimeR() {
    }

    @Test
    void getSelectTasksOrderedByUser() {
    }

    @Test
    void getSelectTasksOrderedByTime() {
    }

    @Test
    void getSelectTasksOrderedByTimeR() {
    }

    @Test
    void refuseTaskInstance() {
    }

    @Test
    void acceptTaskInstance() {
    }

    @Test
    void getTaskInstanceByUserIdR() {
    }

    @Test
    void searchTaskInstanceByUserId() {
    }

    @Test
    void searchTaskInstanceByUserIdR() {
    }

    @Test
    void selectAllTasksOrderedByUser() {
    }

    @Test
    void selectAllTasksOrderedByTime() {
    }

    @Test
    void selectAllTasksOrderedByTimeR() {
    }

    @Test
    void refuseChildrenTaskInstance() {
    }

    @Test
    void acceptChildrenTaskInstance() {
    }

    @Test
    void setResult() {
        long id=1;
        List<ChildrenTaskInstance> childrenTaskInstance=taskMapper.getChildrenTaskInstancesByParentId(id);
        taskMapper.setResult(childrenTaskInstance.get(0).getId(),"hello.png");
        assertEquals("hello.png",taskMapper.getChildrenTaskInstanceById(childrenTaskInstance.get(0).getId()).getOutput());
    }

    @Test
    void childrenTaskInstanceFinished() {
    }

    @Test
    void taskInstanceFinished() {
    }
}