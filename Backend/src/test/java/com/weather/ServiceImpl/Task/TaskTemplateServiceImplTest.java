package com.weather.ServiceImpl.Task;

import com.jcraft.jsch.IO;
import com.weather.Dao.TaskMapper;
import com.weather.Dao.TaskTemplateMapper;
import com.weather.PO.Params;
import com.weather.Service.Task.TaskTemplateService;
import com.weather.VO.BaseTaskTemplateForm;
import com.weather.VO.ParamVO;
import com.weather.VO.ResponseVO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class TaskTemplateServiceImplTest {
    @Autowired
    TaskTemplateService taskTemplateService;
    @Autowired
    TaskTemplateMapper taskTemplateMapper;

    @Test
    void addTaskTemplate() {
    }

    @Test
    void updateTaskTemplate() {
    }

    @Test
    void getTaskTemplates() {
    }

    @Test
    void getChildrenTaskTemplates() {
    }

    @Test
    void getBaseTaskTemplates() {
    }

    @Test
    void deleteTaskTemplate() {
    }

    @Test
    void getTaskTemplateInfo() {
    }



}