package com.weather.ServiceImpl.Task;

import com.weather.Service.Task.TaskService;
import com.weather.VO.ResponseVO;
import com.weather.VO.VerifyTaskForm;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
class TaskServiceImplTest {
    @Autowired
    TaskService taskService;

    @Test
    void getTaskInstancesByUserId() {
    }

    @Test
    void addTaskInstance() {
    }

    @Test
    void getTaskInstanceById() {
    }

    @Test
    void getResult() {
    }

    @Test
    void getAllTasks() {
    }

    @Test
    void verifyTask() {
        assertEquals(ResponseVO.buildSuccess().getSuccess(),taskService.verifyTask(16,0).getSuccess());
    }
}