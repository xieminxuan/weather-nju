package com.weather.ServiceImpl.Task;

import com.weather.Dao.AccountMapper;
import com.weather.Dao.TaskMapper;
import com.weather.Dao.TaskTemplateMapper;
import com.weather.PO.*;
import com.weather.Service.Task.TaskService;
import com.weather.VO.*;
import org.apache.commons.exec.*;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.Base64.Encoder;

@Service
public class TaskServiceImpl implements TaskService {

    private final static String SORTTYPE_NOTEXIST="排序类型错误";
    private final static String TASK_WORKING="任务正在执行中";
    private final static String STATE_NOTEXIST="任务状态不存在";
    private final static String TASK_VERIFYING="任务正在审核中";
    private final static String STATE_ERROR="任务未审批";
    private final static String TASK_NOTEXIST="任务不存在";
    private final static String TASKTEMPLATE_NOTEXIST="任务模板不存在";
    private final static String GET_FAULT="任务获取出错";
    private final static String PARAM_FAULT="参数错误";

    @Autowired
    TaskMapper taskMapper;
    @Autowired
    TaskTemplateMapper taskTemplateMapper;
    @Autowired
    AccountMapper accountMapper;
    @Autowired
    AsyncExecute asyncExecute;


    private boolean checkOutlog(String pathAndName){
        ByteArrayOutputStream stdout = new ByteArrayOutputStream();
        ByteArrayOutputStream stderr = new ByteArrayOutputStream();
        PumpStreamHandler psh = new PumpStreamHandler(stdout, stderr);
        DefaultExecutor exec = new DefaultExecutor();
        CommandLine cl = CommandLine.parse("cat " + pathAndName);
        exec.setStreamHandler(psh);
        try{
            exec.execute(cl);
        }catch (IOException e){
            System.out.println("checkOutlog出错");
            e.printStackTrace();
        }
        String[] arr = stdout.toString().split("\n");
        for(String str:arr){
            if(str.startsWith("XXXXX")){
                System.out.println("找到outdata标记：" + str);
                return true;
            }
        }
        return false;
    }

    private void updateTaskInstanceState(List<TaskInstance> taskInstanceList){
        try{
            for(TaskInstance taskInstance: taskInstanceList){
                //当任务为执行中 才要判断任务是否完成
                if(taskInstance.getState()==1){
                    List<ChildrenTaskInstance> childrenTaskInstances=taskMapper.getChildrenTaskInstancesByParentId(taskInstance.getId());
                    int count = 0;
                    for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                        int index=childrenTaskInstance.getOutput().lastIndexOf("/");
                        String fileName=childrenTaskInstance.getOutput().substring(index+1);
                        String filePath="/home/smart/smart-psm-web/"+childrenTaskInstance.getOutput().substring(0,index+1);
                        String[] command={"/bin/sh","-c","find "+filePath+" -name "+fileName};//判断文件是否存在
                        Process process=Runtime.getRuntime().exec(command);
                        InputStream inputStream=process.getInputStream();
                        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
                        while((bufferedReader.readLine())!=null){
                            String pathAndName = filePath + fileName;
                            if(checkOutlog(pathAndName)){
                                count++;
                                taskMapper.childrenTaskInstanceFinished(childrenTaskInstance.getId());
                            }else{
                                //确定生成了outlog文件，表明跑完了，但没有outdata文件。所以执行失败。
                                taskMapper.childrenTaskInstanceFailed(childrenTaskInstance.getId());
                                taskMapper.taskInstanceFailed(taskInstance.getId());
                            }
                        }
                        process.waitFor();
                        inputStream.close();
                        bufferedReader.close();
                    }
                    if(count == childrenTaskInstances.size()){ //每个子任务都完成了
                        taskMapper.taskInstanceFinished(taskInstance.getId());
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public ResponseVO getTaskInstancesByUserId(long userId,String searchText,String sortType,int state,int current){
        try{
        List<TaskInstance> tempTaskInstances = taskMapper.getTaskInstanceByUserId(userId);
        updateTaskInstanceState(tempTaskInstances);

        List<TaskInstance> taskInstances;
        if(state==3){
            if(sortType.equals("time")||sortType.equals("")){
                taskInstances = taskMapper.getTaskInstanceByUserId(userId);
            }else if(sortType.equals("time-reverse")){
                taskInstances = taskMapper.getTaskInstanceByUserId(userId);
                Collections.reverse(taskInstances);
            }else{
                return ResponseVO.buildFailure(SORTTYPE_NOTEXIST);
            }
        }else if(state==0||state==1||state==2||state==-1||state==-2||state==-3){
            if(sortType.equals("time")||sortType.equals("")){
                taskInstances=taskMapper.searchTaskInstanceByUserId(searchText,userId,state);
            }else if(sortType.equals("time-reverse")){
                taskInstances= taskMapper.searchTaskInstanceByUserId(searchText,userId,state);
                Collections.reverse(taskInstances);
            }else{
                return ResponseVO.buildFailure(SORTTYPE_NOTEXIST);
            }
        }else{
            return ResponseVO.buildFailure(STATE_NOTEXIST);
        }

        int max=taskInstances.size();
        List<UserTaskVO> resList = new ArrayList<>();
        if(max!=0) {
            int fromindex = current * 10;
            int toindex = current * 10 + 10;
            List<TaskInstance> taskInstances1;
            if (fromindex >= max) {
                taskInstances1 = null;
            } else if (toindex > max) {
                taskInstances1 = taskInstances.subList(fromindex, max);
            } else {
                taskInstances1 = taskInstances.subList(fromindex, toindex);
            }
            for (TaskInstance taskInstance : taskInstances1) {
                TaskTemplate taskTemplate = taskTemplateMapper.getTaskTemplateById(taskInstance.getTemplateId());
                List<ChildrenTaskTemplate> childrenTaskTemplates = taskTemplateMapper.getChildrenTaskTemplates(taskInstance.getTemplateId());
                List<String> subnames = new ArrayList<>();
                for (ChildrenTaskTemplate childrenTaskTemplate : childrenTaskTemplates) {
                    BaseTaskTemplate baseTaskTemplate = taskTemplateMapper.getBaseTaskTemplateById(childrenTaskTemplate.getBaseId());
                    subnames.add(baseTaskTemplate.getName());
                }
                UserTaskVO userTaskVO = new UserTaskVO(taskInstance.getId(), taskInstance.getTime(), taskInstance.getName(), taskTemplate.getId(), taskTemplate.getName(), subnames, taskInstance.getState());
                resList.add(userTaskVO);
            }
        }
            UserTaskVOPage userTaskVOPage = new UserTaskVOPage(max, resList);
            return ResponseVO.buildSuccess(userTaskVOPage);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure(TASK_NOTEXIST);
        }
    }




    @Override
    public ResponseVO getAllTasks(String searchText,String sortType,int state,int current){
        try{
            List<TaskInstance> tempTaskInstances = taskMapper.getAllTasks();

            updateTaskInstanceState(tempTaskInstances);

            List<TaskInstance> taskInstances;
            if(state==3){ //state==3为全部
                if(sortType.equals("user")){
                    taskInstances = taskMapper.getAllTasksOrderedByUser(searchText);
                }else if(sortType.equals("time")||sortType.equals("")){
                    taskInstances = taskMapper.getAllTasksOrderedByTime(searchText);
                }else if(sortType.equals("time-reverse")){
                    taskInstances = taskMapper.getAllTasksOrderedByTime(searchText);
                    Collections.reverse(taskInstances);
                }else{
                    return ResponseVO.buildFailure(SORTTYPE_NOTEXIST);
                }
            }else if(state==0||state==1||state==2||state==-1||state==-2 || state==-3){
                if(sortType.equals("user")){
                    taskInstances=taskMapper.selectAllTasksOrderedByUser(searchText,state);
                }else if(sortType.equals("time")||sortType.equals("")){
                    taskInstances=taskMapper.selectAllTasksOrderedByTime(searchText,state);
                }else if(sortType.equals("time-reverse")){
                    taskInstances=taskMapper.selectAllTasksOrderedByTime(searchText,state);
                    Collections.reverse(taskInstances);
                }else{
                    return ResponseVO.buildFailure(SORTTYPE_NOTEXIST);
                }
            }else {
                return ResponseVO.buildFailure(STATE_NOTEXIST);
            }
            int max = taskInstances.size();
            List<TaskInstanceAbbVO> resList = new ArrayList<>();
            if(max!=0){
                int fromindex = current * 10;
                int toindex = current * 10 + 10;
                List<TaskInstance> taskInstancesByPage;
                if (fromindex >= max) {
                    taskInstancesByPage  = new ArrayList<>();
                } else if (toindex > max) {
                    taskInstancesByPage = taskInstances.subList(fromindex, max);
                } else {
                    taskInstancesByPage = taskInstances.subList(fromindex, toindex);
                }
                for (TaskInstance taskInstance : taskInstancesByPage) {
                    TaskInstance temp = taskMapper.getTaskInstanceById(taskInstance.getId());
                    if (temp!=null) {
                        String username = accountMapper.getUserById(taskInstance.getUid()).getUsername();
                        String templateName = taskTemplateMapper.getTaskTemplateById(taskInstance.getTemplateId()).getName();
                        List<ChildrenTaskTemplate> childrenTaskTemplates = taskTemplateMapper.getChildrenTaskTemplates(taskInstance.getTemplateId());
                        List<String> subnames = new ArrayList<>();
                        for (ChildrenTaskTemplate childrenTaskTemplate : childrenTaskTemplates) {
                            subnames.add(taskTemplateMapper.getBaseTaskTemplateById(childrenTaskTemplate.getBaseId()).getName());
                        }
                        TaskInstanceAbbVO taskInstanceAbbVO = new TaskInstanceAbbVO(taskInstance, username, templateName, subnames);
                        resList.add(taskInstanceAbbVO);
                    }
                }
            }
            TaskInstanceAbbVOPage taskInstanceAbbVOPage = new TaskInstanceAbbVOPage(max,resList);
            return ResponseVO.buildSuccess(taskInstanceAbbVOPage);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure(GET_FAULT);
        }
    }



    @Override
    public ResponseVO addTaskInstance(TaskInstanceForm taskInstanceForm){
        try{
        TaskInstance taskInstance=new TaskInstance(taskInstanceForm);
        taskMapper.addTaskInstance(taskInstance);
        List<ChildrenTaskTemplate> childrenTaskTemplates=taskTemplateMapper.getChildrenTaskTemplates(taskInstance.getTemplateId());
        String regex="\"\"";
        String input = taskInstanceForm.getInput();
        input = input.substring(1,input.length()-1);
        String[] inputs = input.split(regex);
        if(inputs.length!=childrenTaskTemplates.size()){
            return ResponseVO.buildFailure(PARAM_FAULT);
        }else {
            int pos=0;
            for (ChildrenTaskTemplate childrenTaskTemplate : childrenTaskTemplates) {
                String [] every_input = inputs[pos++].split(" ");
                List <Params> params = taskTemplateMapper.getParamsByBaseId(childrenTaskTemplate.getBaseId());

                String test_Finally = createChildTaskInstanceInput(every_input, params);

                ChildrenTaskInstanceForm childrenTaskInstanceForm = new ChildrenTaskInstanceForm(taskInstance.getUid(), taskInstance.getId(), childrenTaskTemplate.getId(), test_Finally, 0);
                ChildrenTaskInstance childrenTaskInstance = new ChildrenTaskInstance(childrenTaskInstanceForm);
                taskMapper.addChildrenTaskInstance(childrenTaskInstance);
            }
        }}catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure(TASKTEMPLATE_NOTEXIST);
        }
        return ResponseVO.buildSuccess();
    }


    private String createChildTaskInstanceInput(String[] every_input, List<Params> params){
        StringBuilder inputFinally = new StringBuilder();
        int pos = 0;
        for(Params temp : params){
            //type为integer_range和list的时候，逻辑一样，只是把用户选的这一个数字直接当作参数值。
            if(temp.getType().equals("list")){
                inputFinally.append(every_input[pos]);
            }else if(temp.getType().equals("integer_range")){
                inputFinally.append(every_input[pos]);
            }else if(temp.getType().equals("select")){ //有大括号需要匹配的
                inputFinally.append(every_input[pos]);
                //inputFinally.append(solveSelectTypeParam(temp.getValue(), every_input[pos]));
            }
            inputFinally.append(" ");
            pos++;
        }
        return inputFinally.substring(0, inputFinally.length() - 1);

    }

    private String solveSelectTypeParam(String value,String chinese){
        String[] values = value.split(" ");
        String res="";
        for(String t:values){
            int f = t.lastIndexOf("{");
            String temp = t.substring(f+1,t.length()-1);
            if(temp.equals(chinese)){
                res = t;
                return res;
            }
        }
        return res;
    }

    @Override
    public ResponseVO deleteTaskInstanceById(long id){
        List<ChildrenTaskInstance> childrenTaskInstances;
        try{
            childrenTaskInstances=taskMapper.getChildrenTaskInstancesByParentId(id);
            for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                taskMapper.deleteChildrenTaskInstance(childrenTaskInstance.getId());
            }
            taskMapper.deleteTaskInstance(id);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseVO.buildSuccess();
    }

    @Override
    public ResponseVO getTaskInstanceById(long id,int role) {
        try{
            TaskInstance taskInstance=taskMapper.getTaskInstanceById(id);
            List<TaskInstance> taskInstanceList = new ArrayList<>(1);
            taskInstanceList.add(taskInstance);

            updateTaskInstanceState(taskInstanceList);
            taskInstance = taskMapper.getTaskInstanceById(id);

            User user = accountMapper.getUserById(taskInstance.getUid());
            List<ChildrenTaskInstance> childrenTaskInstances = taskMapper.getChildrenTaskInstancesByParentId(id);
            List<ChildrenTaskInfoDetail> childrenTaskInfoDetailList = new ArrayList<>();
            for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                ChildrenTaskTemplate childrenTaskTemplate = taskTemplateMapper.getChildrenTaskTemplateById(childrenTaskInstance.getChildrenTemplateId());//子任务模板
                String baseTemplate_Name = taskTemplateMapper.getBaseTaskTemplateById(childrenTaskTemplate.getBaseId()).getName();
                List<Params> paramList=taskTemplateMapper.getParamsByBaseId(childrenTaskTemplate.getBaseId());//参数模板
                String[] input=childrenTaskInstance.getInput().split(" ");//输入
                List<TaskParamVO> params=new ArrayList<>();//存储输入参数
                int i=0;
                for(Params params1:paramList){
                    TaskParamVO taskParamVO;
                    if(params1.getType().equals("list")) {
                        taskParamVO = new TaskParamVO(params1.getName(),params1.getCname(), input[i]);
                    }else if(params1.getType().equals("integer_range")){
                        taskParamVO = new TaskParamVO(params1.getName(), params1.getCname(), input[i]);
                    }else{ //type = select
                        String temp="";
                        String in=input[i];
                        if(role==0){//user
                            temp = in.substring(in.lastIndexOf("{")+1,in.lastIndexOf("}"));
                        }else if(role==1){//admin
                            temp=in;
                        }
                        taskParamVO = new TaskParamVO(params1.getName(), params1.getCname(),temp);
                    }
                    params.add(taskParamVO);
                    i++;
                }
                ChildrenTaskInfoDetail childrenTaskInfoDetail = new ChildrenTaskInfoDetail(baseTemplate_Name,childrenTaskInstance.getState(), params);
                childrenTaskInfoDetailList.add(childrenTaskInfoDetail);
        }
            TaskInstanceDetailVO taskInstanceDetailVO=new TaskInstanceDetailVO(id, user.getUsername(), taskInstance.getTime(), taskInstance.getName(), taskInstance.getState(), childrenTaskInfoDetailList);
            return ResponseVO.buildSuccess(taskInstanceDetailVO);

        }catch (Exception e){
            return ResponseVO.buildFailure(TASK_NOTEXIST);
        }
    }


    /**
            *返回结果图base64编码字符串，若没有返回""
            * @param fileName childrentaskinstance的outlog中获取的oudata的地址
     * @return 字符串 base64编码 多个图片用""隔开
     */
    private String getPicBase64(String fileName){
        String res="";
        try{
            String fileType = fileName.substring(fileName.lastIndexOf('.')+1);
            //传输图片
            if(fileType.equals("png")){
                InputStream inputStream = null;
                byte[] data=null;
                try{
                    inputStream = new FileInputStream("/home/smart/smart-psm-web/" + fileName);
                    data = new byte[inputStream.available()];
                    inputStream.read(data);
                    inputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
                //加密
                Encoder encoder= Base64.getEncoder();
                res = encoder.encodeToString(data);
                //res=res+"\""+encoder.encodeToString(data)+"\"";
            }
                return res;
        }catch (Exception e){
            e.printStackTrace();
        }
        return res;

    }


    /*
        id：taskInstance的id
     */
    @Override
    public ResponseVO getFiles(long taskInstance_id){
        List<FileAbbr> fileAbbrs = new ArrayList<>();
        try {
            List<ChildrenTaskInstance> childrenTaskInstances = taskMapper.getChildrenTaskInstancesByParentId(taskInstance_id);
            ChildrenTaskInstance final_childrenTaskInstance = childrenTaskInstances.get(childrenTaskInstances.size()-1);
            long childTaskTemplate_id = final_childrenTaskInstance.getChildrenTemplateId();
            String filePath="/home/smart/smart-psm-web/";
            String outlog_name = final_childrenTaskInstance.getOutput();
            List<String> res = checkOutDataFile(filePath+outlog_name);
            String childrenTaskTemplateName = taskTemplateMapper.getBaseTaskTemplateById(taskTemplateMapper.getChildrenTaskTemplateById(final_childrenTaskInstance.getChildrenTemplateId()).getBaseId()).getName();
            for(String outData_name : res) {
                String picBase64 = getPicBase64(outData_name);
                FileAbbr fileAbbr = new FileAbbr(childTaskTemplate_id, childrenTaskTemplateName, outData_name, picBase64);
                fileAbbrs.add(fileAbbr);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseVO.buildSuccess(fileAbbrs);
    }


    private List<String> checkOutDataFile(String pathAndName){
        int find = 0;
        List<String> outDataFileNameList = new ArrayList<>();
        ByteArrayOutputStream stdout = new ByteArrayOutputStream();
        ByteArrayOutputStream stderr = new ByteArrayOutputStream();
        PumpStreamHandler psh = new PumpStreamHandler(stdout, stderr);
        DefaultExecutor exec = new DefaultExecutor();
        CommandLine cl = CommandLine.parse("cat " + pathAndName);
        exec.setStreamHandler(psh);
        try{
            exec.execute(cl);
        }catch (IOException e){
            System.out.println("checkOutlog出错");
            e.printStackTrace();
        }
        String regx = "//";
        String[] arr = stdout.toString().split("\n");
        for(String str:arr){
            if(find > 0){
                int indexForOutData = str.indexOf(regx) + 2;
                outDataFileNameList.add(str.substring(indexForOutData));
            }
            if(str.startsWith("XXXXX") && find==0){
                find++;
            }
        }
        return outDataFileNameList;
    }


    /**
     * 管理员审核用户请求进行的任务
     * @param id 任务的id
     * @param verifyType 0代表审核通过，1代表拒绝
     * @return
     */
    @Override
    public ResponseVO verifyTask(long id,int verifyType){
        if(verifyType==1){
            try {
                List<ChildrenTaskInstance> childrenTaskInstances = taskMapper.getChildrenTaskInstancesByParentId(id);
                if(childrenTaskInstances.size() == 0) return ResponseVO.buildFailure(TASK_NOTEXIST);
                for (ChildrenTaskInstance childrenTaskInstance : childrenTaskInstances) {
                    taskMapper.refuseChildrenTaskInstance(childrenTaskInstance.getId());
                }
                taskMapper.refuseTaskInstance(id);
            }catch (Exception e){
                e.printStackTrace();
                return ResponseVO.buildFailure(TASK_NOTEXIST);
            }
        }else{
            try{
                taskMapper.acceptTaskInstance(id);
                List<ChildrenTaskInstance> childrenTaskInstances=taskMapper.getChildrenTaskInstancesByParentId(id);//子任务进程
                if(childrenTaskInstances.size() == 0)
                    return ResponseVO.buildFailure(TASK_NOTEXIST);
                for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                    taskMapper.acceptChildrenTaskInstance(childrenTaskInstance.getId());
                }
            }catch (Exception e){
                e.printStackTrace();
                return ResponseVO.buildFailure(TASK_NOTEXIST);
            }
        }
        return ResponseVO.buildSuccess();
    }

    /**
     * 管理员执行该任务。 -2为审核通过但未执行。1为执行中。
     * @param id
     * @return
     */
    @Override
    public ResponseVO executeTask(long id){
        try{
            System.out.println("executeTaskService开始");
            int state = taskMapper.getTaskInstanceById(id).getState();
            if(state==-2||state==1) {
                taskMapper.runTaskInstance(id); //更新state
                //获取该任务下的所有子任务
                List<ChildrenTaskInstance> childrenTaskInstances=taskMapper.getChildrenTaskInstancesByParentId(id);
                for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                    taskMapper.runChildrenTaskInstance(childrenTaskInstance.getId()); //更新state
                    BaseTaskTemplate baseTaskTemplate=taskTemplateMapper.getBaseTaskTemplateById(taskTemplateMapper.getChildrenTaskTemplateById(childrenTaskInstance.getChildrenTemplateId()).getBaseId());
                    String outLogName = baseTaskTemplate.getOutLogName(); //输出的日志文件的名字
                    List<Params> params=taskTemplateMapper.getParamsByBaseId(taskTemplateMapper.getChildrenTaskTemplateById(childrenTaskInstance.getChildrenTemplateId()).getBaseId());
                    int i=0;
                    String[] input=taskMapper.getChildrenTaskInstanceById(childrenTaskInstance.getId()).getInput().split(" ");//输入
                    String res;
                    String varname="";
                    for(Params param:params){
                        String regex="{"+param.getName()+"}";
                        String replacement;
                        if(param.getType().equals("list")){
                            replacement = input[i];
                        }else if(param.getType().equals("integer_range")){
                            replacement = input[i];
                        } else {
                            replacement = input[i].substring(0, input[i].lastIndexOf("{"));
                        }
                        varname = varname + " " + replacement;
                        i++;
                        res = outLogName.replace(regex,replacement);
                        outLogName = res;
                    }
                    taskMapper.setResult(childrenTaskInstance.getId(), outLogName);
                    String filePath = "/home/smart/smart-psm-web/";
                    String fileName = baseTaskTemplate.getScriptName() + varname;
                    String scriptCommand = "sh /home/smart/smart-psm-web/start.sh " + filePath + " " + fileName;
                    System.out.println("----------");
                    System.out.println("调用命令为：" + scriptCommand);
                    System.out.println("----------");
                    ByteArrayOutputStream stdout = new ByteArrayOutputStream();
                    ByteArrayOutputStream stderr = new ByteArrayOutputStream();
                    PumpStreamHandler psh = new PumpStreamHandler(stdout, stderr);
                    CommandLine cl = CommandLine.parse(scriptCommand);
                    //设置2分钟超时
                    ExecuteWatchdog watchdog = new ExecuteWatchdog(120 * 1000);
                    DefaultExecutor exec = new DefaultExecutor();
                    //exec.setExitValue(1); //设置命令执行退出值为1，如果命令成功执行并且没有错误，则返回1
                    exec.setWatchdog(watchdog);
                    exec.setStreamHandler(psh);
                    try {
                        exec.execute(cl);
                    } catch (IOException e) {
                        if(watchdog.killedProcess()){
                            System.out.println("超时");
                        }
                        e.printStackTrace();
                    }
                }
            }else{
                System.out.println("任务状态不为-2或1，executeTaskService结束--失败");
            }
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("executeTaskService结束--失败");
            return ResponseVO.buildFailure("执行出错");
        }
        System.out.println("executeTaskService结束--成功");
        return ResponseVO.buildSuccess();
    }
}
