package com.weather.ServiceImpl.Task;

import com.weather.Dao.AccountMapper;
import com.weather.Dao.TaskMapper;
import com.weather.Dao.TaskTemplateMapper;
import com.weather.PO.BaseTaskTemplate;
import com.weather.PO.ChildrenTaskInstance;
import com.weather.PO.Params;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class AsyncExecute {

    @Autowired
    TaskMapper taskMapper;
    @Autowired
    TaskTemplateMapper taskTemplateMapper;
    @Autowired
    AccountMapper accountMapper;
    /*
        异步执行脚本，暂时弃用
     */
    @Async
    public void executeTask(long id){
        try{
            System.out.println("executeTaskService开始");
            int state = taskMapper.getTaskInstanceById(id).getState();
            if(state==-2||state==1) {
                taskMapper.runTaskInstance(id); //更新state
                //获取该任务下的所有子任务
                List<ChildrenTaskInstance> childrenTaskInstances=taskMapper.getChildrenTaskInstancesByParentId(id);
                for(ChildrenTaskInstance childrenTaskInstance:childrenTaskInstances){
                    taskMapper.runChildrenTaskInstance(childrenTaskInstance.getId()); //更新state
                    BaseTaskTemplate baseTaskTemplate=taskTemplateMapper.getBaseTaskTemplateById(taskTemplateMapper.getChildrenTaskTemplateById(childrenTaskInstance.getChildrenTemplateId()).getBaseId());
                    String outLogName = baseTaskTemplate.getOutLogName(); //输出的日志文件的名字
                    List<Params> params=taskTemplateMapper.getParamsByBaseId(taskTemplateMapper.getChildrenTaskTemplateById(childrenTaskInstance.getChildrenTemplateId()).getBaseId());
                    int i=0;
                    String[] input=taskMapper.getChildrenTaskInstanceById(childrenTaskInstance.getId()).getInput().split(" ");//输入
                    String res;
                    String varname="";
                    for(Params param:params){
                        String regex="{"+param.getName()+"}";
                        String replacement;
                        if(param.getType().equals("list")){
                            replacement = input[i];
                        }else if(param.getType().equals("integer_range")){
                            replacement = input[i];
                        } else {
                            replacement = input[i].substring(0, input[i].lastIndexOf("{"));
                        }
                        varname = varname + " " + replacement;
                        i++;
                        res = outLogName.replace(regex,replacement);
                        outLogName = res;
                    }
                    taskMapper.setResult(childrenTaskInstance.getId(), outLogName);
                    String filePath = "/home/smart/smart-psm-web/";
                    String fileName = baseTaskTemplate.getScriptName() + varname;
                    String scriptCommand = "sh /home/smart/smart-psm-web/start.sh " + filePath + " " + fileName;
                    System.out.println("----------");
                    System.out.println("调用命令为：" + scriptCommand);
                    System.out.println("----------");
                    ByteArrayOutputStream stdout = new ByteArrayOutputStream();
                    ByteArrayOutputStream stderr = new ByteArrayOutputStream();
                    PumpStreamHandler psh = new PumpStreamHandler(stdout, stderr);
                    CommandLine cl = CommandLine.parse(scriptCommand);
                    //设置2分钟超时
                    ExecuteWatchdog watchdog = new ExecuteWatchdog(120 * 1000);
                    DefaultExecutor exec = new DefaultExecutor();
                    //exec.setExitValue(1); //设置命令执行退出值为1，如果命令成功执行并且没有错误，则返回1
                    exec.setWatchdog(watchdog);
                    exec.setStreamHandler(psh);
                    int exitValue = -100;
                    try {
                        exitValue = exec.execute(cl);
                    } catch (IOException e) {
                        if(watchdog.killedProcess()){
                            System.out.println("超时");
                        }
                        e.printStackTrace();
                    }
                    System.out.println("exitValue : " + exitValue);
                }
            }else{
                System.out.println("executeTaskService结束--失败");
            }
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("executeTaskService结束--失败");
        }
        System.out.println("executeTaskService结束--成功");
    }
}
