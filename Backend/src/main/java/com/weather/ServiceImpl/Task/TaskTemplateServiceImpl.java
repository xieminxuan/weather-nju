package com.weather.ServiceImpl.Task;

import com.weather.Dao.TaskTemplateMapper;
import com.weather.PO.*;
import com.weather.Service.Task.TaskTemplateService;
import com.weather.VO.*;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.*;
import java.security.spec.ECField;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TaskTemplateServiceImpl implements TaskTemplateService {

    private final static String BASETEMPLATE_NOTEXIST = "子任务模板不存在";
    private final static String TEMPLATE_NOTEXIST = "任务模板不存在";

    @Autowired
    TaskTemplateMapper taskTemplateMapper;

    @Override
    public ResponseVO getTaskTemplates(String searchText, int current){
        List<TaskTemplate> taskTemplates;
        if(searchText.equals("")) {
            taskTemplates=taskTemplateMapper.getTaskTemplates();
        }else{
            taskTemplates=taskTemplateMapper.selectTaskTemplates(searchText);
        }
        int max=taskTemplates.size();
        List<TaskTemplateVO> resList=new ArrayList<>();
        if(max!=0) {
            int fromindex = current * 10;
            int toindex = current * 10 + 10;
            List<TaskTemplate> taskTemplates1;
            if (fromindex >= max) {
                taskTemplates1 = null;
            } else if (toindex > max) {
                taskTemplates1 = taskTemplates.subList(fromindex, max);
            } else {
                taskTemplates1 = taskTemplates.subList(fromindex, toindex);
            }
            for (TaskTemplate taskTemplate : taskTemplates1) {
                resList.add(new TaskTemplateVO(taskTemplate));
            }
        }
        TaskTemplateVOPage taskTemplateVOPage=new TaskTemplateVOPage(max,resList);
        return ResponseVO.buildSuccess(taskTemplateVOPage);
    }


    @Override
    public ResponseVO getChildrenTaskTemplates(long parentId){
        try {
            List<ChildrenTaskTemplate> childrenTaskTemplates = taskTemplateMapper.getChildrenTaskTemplates(parentId);
            List<ChildrenTaskTemplateDetailVO> childrenTaskTemplateDetailVOs = new ArrayList<>();

            for (ChildrenTaskTemplate childrenTaskTemplate : childrenTaskTemplates) {
                BaseTaskTemplate baseTaskTemplate = taskTemplateMapper.getBaseTaskTemplateById(childrenTaskTemplate.getBaseId());
                List<Params> paramsList = taskTemplateMapper.getParamsByBaseId(childrenTaskTemplate.getBaseId());
                List<ParamVO> params = new ArrayList<>();
                for (Params params1 : paramsList) {
                    /*
                        value即取出注释的内容
                     */
                    String value="";
                    if(params1.getType().equals("list") || params1.getType().equals("integer_range")){
                        value=params1.getValue();
                    }else if(params1.getType().equals("select")){
                        /*
                        String[] temp=params1.getValue().split("}");
                        for(String t:temp){
                            value=value+t.substring(t.lastIndexOf("{")+1)+" ";
                        }
                        value=value.substring(0,value.length()-1);
                         */
                        value = params1.getValue();
                    }
                    Params tempParam = new Params(params1.getId(),params1.getBaseId(),params1.getType(),value,params1.getName(),params1.getCname(),params1.getIndex());
                    ParamVO paramVO = new ParamVO(tempParam);
                    params.add(paramVO);
                }
                ChildrenTaskTemplateDetailVO childrenTaskTemplateDetailVO = new ChildrenTaskTemplateDetailVO(childrenTaskTemplate.getId(), baseTaskTemplate.getName(), baseTaskTemplate.getDescription(), childrenTaskTemplate.getIndex(), baseTaskTemplate.getScriptName(), params);
                childrenTaskTemplateDetailVOs.add(childrenTaskTemplateDetailVO);
            }
            return ResponseVO.buildSuccess(childrenTaskTemplateDetailVOs);
        }catch (Exception e){
            return ResponseVO.buildFailure(TEMPLATE_NOTEXIST);
        }
    }

    @Override
    public ResponseVO addTaskTemplate(long id, String name,String description,String baseTasks){
        TaskTemplate taskTemplate = new TaskTemplate(name,description,0);
        long add_id = -1;
        if(id!=-1) {//id!=-1 则为更新模版操作。
            taskTemplateMapper.updateTaskTemplateByIsDelete(id);
        }
        taskTemplateMapper.addTaskTemplate(taskTemplate);
        add_id = taskTemplateMapper.getTaskTemplateByName(name).getId();
        //无论新增还是修改，都直接add子任务模版
        int i=1;
        String[] baseTaskList = baseTasks.split(",");
        for(String baseTask:baseTaskList){
            long baseId=Long.parseLong(baseTask);
            if(taskTemplateMapper.getBaseTaskTemplateById(baseId)!=null){
                ChildrenTaskTemplate childrenTaskTemplate = new ChildrenTaskTemplate(add_id, i++, baseId);
                taskTemplateMapper.addChildrenTaskTemplate(childrenTaskTemplate);
            }else{
                return ResponseVO.buildFailure(BASETEMPLATE_NOTEXIST);
            }
        }
        return ResponseVO.buildSuccess();
    }


    @Override
    public ResponseVO getBaseTaskTemplates(String searchText,int current){
        List<BaseTaskTemplate> baseTaskTemplates;
        if(searchText.equals("")) {
            baseTaskTemplates=taskTemplateMapper.getBaseTaskTemplates();
        }else{
            baseTaskTemplates=taskTemplateMapper.selectBaseTaskTemplates(searchText);
        }
        int max=baseTaskTemplates.size();
        List<BaseTaskTemplateBrief> resList= new ArrayList<>();
        if(max!=0) {
            int fromindex = current * 10;
            int toindex = current * 10 + 10;
            List<BaseTaskTemplate> baseTaskTemplates1;
            if (fromindex >= max) {
                baseTaskTemplates1 = null;
            } else if (toindex > max) {
                baseTaskTemplates1 = baseTaskTemplates.subList(fromindex, max);
            } else {
                baseTaskTemplates1 = baseTaskTemplates.subList(fromindex, toindex);
            }
            for (BaseTaskTemplate baseTaskTemplate : baseTaskTemplates1) {
                List<Params> paramsList = taskTemplateMapper.getParamsByBaseId(baseTaskTemplate.getId());
                List<ParamVO> params = new ArrayList<>();
                for (Params params1 : paramsList) {
                    ParamVO paramVO = new ParamVO(params1);
                    params.add(paramVO);
                }
                BaseTaskTemplateBrief baseTaskTemplateBrief = new BaseTaskTemplateBrief(baseTaskTemplate.getId(), baseTaskTemplate.getName(), baseTaskTemplate.getDescription(), baseTaskTemplate.getScriptName(), params, baseTaskTemplate.getOutLogName());
                resList.add(baseTaskTemplateBrief);
            }
        }
        BaseTaskTemplateBriefPage baseTaskTemplateBriefPage = new BaseTaskTemplateBriefPage(max,resList);
        return ResponseVO.buildSuccess(baseTaskTemplateBriefPage);
    }

    @Override
    public ResponseVO getTaskTemplateInfo(long id){
        try{
            TaskTemplate taskTemplate=taskTemplateMapper.getTaskTemplateById(id);
            List<ChildrenTaskTemplate> childrenTaskTemplateList=taskTemplateMapper.getChildrenTaskTemplates(id);
            List<ChildrenTaskTemplateInfo> childrenTaskTemplates=new ArrayList<>();
            for(ChildrenTaskTemplate childrenTaskTemplate:childrenTaskTemplateList){
                BaseTaskTemplate baseTaskTemplate=taskTemplateMapper.getBaseTaskTemplateById(childrenTaskTemplate.getBaseId());
                List<Params> paramsList=taskTemplateMapper.getParamsByBaseId(childrenTaskTemplate.getBaseId());
                List<ParamVO> params=new ArrayList<>();
                for(Params params1:paramsList){
                    ParamVO paramVO=new ParamVO(params1);
                    params.add(paramVO);
                }
                ChildrenTaskTemplateInfo childrenTaskTemplateInfo=new ChildrenTaskTemplateInfo(baseTaskTemplate,params,childrenTaskTemplate.getIndex());
                childrenTaskTemplates.add(childrenTaskTemplateInfo);
            }
            TaskTemplateInfo taskTemplateInfo=new TaskTemplateInfo(taskTemplate,childrenTaskTemplates);
            return ResponseVO.buildSuccess(taskTemplateInfo);
        }catch (Exception e){
            return ResponseVO.buildFailure(TEMPLATE_NOTEXIST);
        }
    }

    @Override
    public ResponseVO addBaseTaskTemplate(BaseTaskTemplateForm baseTaskTemplateForm){
        long new_id=-1;
        try{
            BaseTaskTemplate baseTaskTemplate = new BaseTaskTemplate(baseTaskTemplateForm.getName(),baseTaskTemplateForm.getDescription(), baseTaskTemplateForm.getScriptName(), baseTaskTemplateForm.getOutLogName(),0);

            if(baseTaskTemplateForm.getId() == -1){//新增脚本
                BaseTaskTemplate baseTaskTemplate_testReName = taskTemplateMapper.getBaseTaskTemplateByName(baseTaskTemplateForm.getName());
                if(baseTaskTemplate_testReName!=null)
                    return ResponseVO.buildFailure("Fail to add BaseTaskTemplate.(名称重复)");
                taskTemplateMapper.addBaseTaskTemplate(baseTaskTemplate);
                long baseId = taskTemplateMapper.getBaseTaskTemplateByName(baseTaskTemplateForm.getName()).getId();
                List<ParamVO> paramVO_list = baseTaskTemplateForm.getParams();
                int i=1;
                for(ParamVO paramsVO:paramVO_list){
                    Params params = new Params(baseId,paramsVO.getType(),paramsVO.getValues(),paramsVO.getName(),paramsVO.getCname(),i++);
                    taskTemplateMapper.addParam(params);
                }
                new_id = baseId;
            }else{ //修改脚本
                int num = taskTemplateMapper.updateBaseTaskTemplateByIsDelete(baseTaskTemplateForm.getId());
                if(num!=1){
                    System.out.println("更新isDelete失败");
                    return ResponseVO.buildFailure("更新脚本状态失败。");
                }
                taskTemplateMapper.addBaseTaskTemplate(baseTaskTemplate);
                long baseId = taskTemplateMapper.getBaseTaskTemplateByName(baseTaskTemplateForm.getName()).getId();
                List<ParamVO> paramVO_list = baseTaskTemplateForm.getParams();
                int i=1;
                for(ParamVO paramsVO:paramVO_list){
                    Params params = new Params(baseId,paramsVO.getType(),paramsVO.getValues(),paramsVO.getName(),paramsVO.getCname(),i++);
                    taskTemplateMapper.addParam(params);
                }
                new_id = baseId;
                long old_base_id = baseTaskTemplateForm.getId();
                List<ChildrenTaskTemplate> list = taskTemplateMapper.getChildrenTaskTemplatesByBaseId(old_base_id);
                Set<Long> taskTemplates_to_update = new HashSet<>();
                for(ChildrenTaskTemplate childrenTaskTemplate : list){
                    TaskTemplate taskTemplate = taskTemplateMapper.getTaskTemplateById(childrenTaskTemplate.getParentId());
                    taskTemplates_to_update.add(taskTemplate.getId());
                }

                for(Long taskTemplate_id : taskTemplates_to_update){
                    TaskTemplate taskTemplate = taskTemplateMapper.getTaskTemplateById(taskTemplate_id);
                    List<ChildrenTaskTemplate> childrenTaskTemplateList = taskTemplateMapper.getChildrenTaskTemplates(taskTemplate.getId());
                    StringBuilder baseTasks = new StringBuilder();
                    for(ChildrenTaskTemplate temp : childrenTaskTemplateList){
                        if(temp.getBaseId() == old_base_id){
                            baseTasks.append(new_id);
                        }else{
                            baseTasks.append(temp.getBaseId());
                        }
                        baseTasks.append(",");
                    }
                    baseTasks.deleteCharAt(baseTasks.length()-1);//把最后的逗号 删掉。
                    addTaskTemplate(taskTemplate.getId(), taskTemplate.getName(), taskTemplate.getDescription() , baseTasks.toString());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure("Fail to add BaseTaskTemplate.");
        }
        return ResponseVO.buildSuccess(new_id);
    }

    @Override
    public ResponseVO deleteTaskTemplate(long id){
        try {
            taskTemplateMapper.deleteTaskTemplate(id);
            taskTemplateMapper.deleteChildrenTemplates(id);
        }catch (Exception e){
            return ResponseVO.buildFailure(TEMPLATE_NOTEXIST);
        }
        return ResponseVO.buildSuccess();
    }


    @Override
    public ResponseVO deleteBaseTaskTemplate(long id){
        try {
            taskTemplateMapper.deleteParamsByBaseId(id);
            taskTemplateMapper.deleteBaseTaskTemplate(id);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure("Fail to delete BaseTaskTemplate.");
        }
        return ResponseVO.buildSuccess();
    }


    @Override
    public ResponseVO uploadFile(MultipartFile multipartFile,long id){
        try{
            String fileName=multipartFile.getOriginalFilename();//获取文件名
            String filePath="/home/smart/smart-psm-web/"+fileName;//文件路径
            File targetFile=new File(filePath);
            //目录不存在
            if(!targetFile.getParentFile().exists()){
                targetFile.getParentFile().mkdirs();
            }
            File file1=new File(filePath);
            try{
                if(!file1.exists()){
                    file1.createNewFile();
                }
                FileOutputStream outputStream=new FileOutputStream(file1);
                byte[] bytes=multipartFile.getBytes();
                outputStream.write(bytes);
                outputStream.close();
            }catch (IOException e){
                e.printStackTrace();
            }
            taskTemplateMapper.uploadFile(id,multipartFile.getOriginalFilename());
        }catch (Exception e){
            e.printStackTrace();
            return ResponseVO.buildFailure("Fail to upload the file.");
        }
        return ResponseVO.buildSuccess();
    }

}
