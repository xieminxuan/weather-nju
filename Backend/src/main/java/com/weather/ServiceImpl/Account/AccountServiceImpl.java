package com.weather.ServiceImpl.Account;

import com.weather.Dao.AccountMapper;
import com.weather.PO.Admin;
import com.weather.PO.User;
import com.weather.Service.Account.AccountService;
import com.weather.VO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final static String ACCOUNT_EXIST="账号已存在";
    private final static String STATE_NOTEXIST="用户状态不存在";

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public UserVO loginUser(UserForm userForm){
        User user=accountMapper.getUserByName(userForm.getUsername());
        if(user==null||!user.getPassword().equals(userForm.getPassword())){
            return null;
        }
        return new UserVO(user);
    }

    @Override
    public UserVO getUserInfo(long id){
        User user=accountMapper.getUserById(id);
        if(user==null){
            return null;
        }
        return new UserVO(user);
    }

    @Override
    public AdminVO loginAdmin(UserForm userForm){
        Admin admin=accountMapper.getAdminByName(userForm.getUsername());
        if(admin==null||!admin.getPassword().equals(userForm.getPassword())){
            return null;
        }
        return new AdminVO(admin);
    }

    @Override
    public ResponseVO addUser(UserForm userForm){
        try{
            int flag=0;
            /*
            User user1 = accountMapper.getUserByName(userForm.getUsername());
            if(user1!=null) return ResponseVO.buildFailure(ACCOUNT_EXIST);
            else{
                User user=new User(userForm);
                user.setState(1); //状态设置为正常
                accountMapper.addUser(user);
            }
            */

            List<User> users=accountMapper.getAllUsers();
            for(int i=0;i<users.size();i++){
                if(userForm.getUsername().equals(users.get(i).getUsername())){
                    flag=1;
                    break;
                }
            }
            if(flag==1){
                return ResponseVO.buildFailure(ACCOUNT_EXIST);
            }else{
                User user=new User(userForm);
                user.setState(1); //状态设置为正常
                accountMapper.addUser(user);
            }
        }catch (Exception e){
            return ResponseVO.buildFailure(ACCOUNT_EXIST);
        }
        return ResponseVO.buildSuccess();
    }

    @Override
    public ResponseVO getAllUsers(String searchText, int state, int current){
        //state 按照用户账户是否冻结状态筛选 1正常 2冻结 3全部

        List<User> users=new ArrayList<>();

        if(state==3){
            if (searchText == "") {
                users=accountMapper.getAllUsers();
            }else {
                users = accountMapper.searchAllUsers(searchText);
            }
        }else if(state==1||state==2){
            if(searchText==""){
                users=accountMapper.getAllStateUsers(state);
            }else{
                users=accountMapper.searchAllStateUsers(searchText,state);
            }
        }else{
            return ResponseVO.buildFailure(STATE_NOTEXIST);
        }

        int max=users.size();
        List<User> resList=new ArrayList<>();
        if(max!=0) {
            int fromindex = current * 10;
            int toindex = current * 10 + 10;

            if (fromindex >= max) {
                resList = null;
            } else if (toindex > max) {
                resList = users.subList(fromindex, max);
            } else {
                resList = users.subList(fromindex, toindex);
            }
        }
        UserPage userPage=new UserPage(max,resList);
        return ResponseVO.buildSuccess(userPage);
    }

    @Override
    public ResponseVO changeState(StateForm stateForm){
        return ResponseVO.buildSuccess(accountMapper.changeState(stateForm.getId(),stateForm.getState()));
    }
}
