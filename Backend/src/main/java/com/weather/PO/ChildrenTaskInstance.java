package com.weather.PO;

import com.weather.VO.ChildrenTaskInstanceForm;

public class ChildrenTaskInstance {
    private long id;
    private long uid;
    private long parentId;
    private long childrenTemplateId;
    private String input;
    private int state;
    private String output;

    public ChildrenTaskInstance(){}

    public ChildrenTaskInstance(ChildrenTaskInstanceForm childrenTaskInstanceForm){
        this.uid=childrenTaskInstanceForm.getUid();
        this.parentId=childrenTaskInstanceForm.getParentId();
        this.childrenTemplateId=childrenTaskInstanceForm.getChildrenTemplateId();
        this.input=childrenTaskInstanceForm.getInput();
        this.state=childrenTaskInstanceForm.getState();
        this.output="";
    }

    public ChildrenTaskInstance(long id,long uid,long parentId,long childrenTemplateId,String input,int state,String output){
        this.id=id;
        this.uid=uid;
        this.parentId=parentId;
        this.childrenTemplateId=childrenTemplateId;
        this.input=input;
        this.state=state;
        this.output=output;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public void setChildrenTemplateId(long childrenTemplateId) {
        this.childrenTemplateId = childrenTemplateId;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void setState(int state) {
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public long getUid() {
        return uid;
    }

    public int getState() {
        return state;
    }

    public long getChildrenTemplateId() {
        return childrenTemplateId;
    }

    public long getParentId() {
        return parentId;
    }

    public String getInput() {
        return input;
    }

    public String getOutput(){return output;}

    public void setOutput(String output){this.output=output;}
}
