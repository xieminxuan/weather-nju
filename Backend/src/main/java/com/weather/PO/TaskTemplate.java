package com.weather.PO;

import com.weather.VO.TaskTemplateInfo;

public class TaskTemplate {
    private long id;
    private String name;
    private String description;
    private int isDelete; //是否删除，1为已经删除，0为正常

    public TaskTemplate(){}
    public TaskTemplate(TaskTemplateInfo taskTemplateForm){
        this.name= taskTemplateForm.getName();
        this.description= taskTemplateForm.getDescription();
    }
    public TaskTemplate(String name,String description, int isDelete){
        this.name= name;
        this.description= description;
        this.isDelete = isDelete;
    }
    public TaskTemplate(long id, String name, String description, int isDelete){
        this.id=id;
        this.name=name;
        this.description=description;
        this.isDelete = isDelete;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public long getId(){return id;}

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description){
        this.description=description;
    }

    public String getName(){return name;}

    public String getDescription(){return description;}
}
