package com.weather.PO;

public class ChildrenTaskTemplate {
    private long id;
    private long parentId;// 所属的任务模版
    private int index; // 子任务所属于的节点 第一个 第二个 第三个
    private long baseId;//对应 数据库中具体脚本的id
    public ChildrenTaskTemplate(){}
    public ChildrenTaskTemplate(long id,long parentId,long baseId, int index){
        this.id=id;
        this.parentId=parentId;
        this.baseId=baseId;
        this.index=index;
    }
    public ChildrenTaskTemplate(long parentId,int index,long baseId ){
        this.parentId=parentId;
        this.baseId=baseId;
        this.index=index;
    }
    public void setId(long id){this.id=id;}

    public void setParentId(long parentId){this.parentId=parentId;}

    public void setBaseId(long baseId){this.baseId=baseId;}

    public void setIndex(int index){this.index=index;}

    public long getId(){return this.id;}

    public long getParentId(){return this.parentId;}

    public long getBaseId(){return this.baseId;}

    public int getIndex(){return this.index;}

}
