package com.weather.PO;

public class BaseTaskTemplate {
    private long id;
    private String name;
    private String description;
    private String scriptName; //这个脚本所调用的 脚本文件的名字
    private String outLogName; //日志文件名
    private int isDelete; //0代表没删除，1代表已经删除，不再使用

    public BaseTaskTemplate(){}
    public BaseTaskTemplate(long id,String name,String description,String scriptName,String outLogName, int isDelete){
        this.id=id;
        this.name=name;
        this.description=description;
        this.scriptName=scriptName;
        this.outLogName = outLogName;
        this.isDelete = isDelete;
    }
    public BaseTaskTemplate(String name,String description,String scriptName, String outLogName, int isDelete){
        this.name=name;
        this.description=description;
        this.scriptName=scriptName;
        this.outLogName = outLogName;
        this.isDelete = isDelete;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getOutLogName() {
        return outLogName;
    }

    public void setOutLogName(String outLogName) {
        this.outLogName = outLogName;
    }
}
