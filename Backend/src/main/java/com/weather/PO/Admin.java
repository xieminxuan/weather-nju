package com.weather.PO;

import com.weather.VO.UserForm;

public class Admin {
    private long id;
    private String username;
    private String password;
    private int state;//是否冻结
    private int role;

    public Admin(){this.role=2;}
    public Admin(UserForm userForm){
        this.username=userForm.getUsername();
        this.password=userForm.getPassword();
        this.state=0;
        this.role=2;
    }
    public long getId(){return id;}

    public String getUsername(){return username;}

    public String getPassword(){return password;}

    public int getState(){return state;}

    public int getRole(){return role;}

    public  void setUsername(String username){this.username=username;}

    public void setPassword(String password){this.password=password;}

    public void setState(int state){this.state=state;}

    public void setRole(int role){this.role=role;}
}
