package com.weather.PO;

public class Params {
    private long id;
    private long baseId;
    private String name;
    private String cname;
    private String type;
    private String value;
    private int index;

    public Params(){}
    public Params(long baseId,String type,String value,String name,String cname,int index){
        this.name=name;
        this.baseId=baseId;
        this.value=value;
        this.index=index;
        this.type=type;
        this.cname=cname;
    }
    public Params(long id,long baseId,String type,String value,String name,String cname,int index){
        this.id=id;
        this.name=name;
        this.baseId=baseId;
        this.type=type;
        this.value=value;
        this.index=index;
        this.cname=cname;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setBaseId(long baseId) {
        this.baseId = baseId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public long getBaseId() {
        return baseId;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
