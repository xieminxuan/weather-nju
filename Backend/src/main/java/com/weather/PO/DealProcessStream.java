package com.weather.PO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DealProcessStream extends Thread{
    private InputStream inputStream;

    public DealProcessStream(InputStream inputStream){
        this.inputStream = inputStream;
    }

    public void run(){
        InputStreamReader inputStreamReader = null;
        BufferedReader br = null;
        try{
            inputStreamReader = new InputStreamReader(inputStream);
            br = new BufferedReader(inputStreamReader);
            String line = null;
            while((line = br.readLine())!=null){
                System.out.println(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                br.close();
                inputStreamReader.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
