package com.weather.PO;

import com.weather.VO.TaskInstanceForm;

public class TaskInstance {
    private long id;
    private long uid;// 发起请求的用户id
    private String name;
    private long templateId;
    private int state;//待定 状态 等待审批 执行中 执行完成等
    private String time;//任务创建时间

    public TaskInstance(){}

    public TaskInstance(TaskInstanceForm taskInstanceForm){
        this.uid=taskInstanceForm.getUid();
        this.name=taskInstanceForm.getName();
        this.templateId=taskInstanceForm.getTemplateId();
        this.state = 0;
        this.time=taskInstanceForm.getTime();
    }

    public TaskInstance(long id,long uid,String name,long templateId, int state,String time){
        this.id=id;
        this.uid=uid;
        this.name=name;
        this.templateId=templateId;
        this.state=state;
        this.time=time;
    }

    public void setId(long id){this.id=id;}

    public void setUid(long uid){this.uid=uid;}

    public void setTemplateId(long templateId){this.templateId=templateId;}

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setState(int state){this.state=state;}

    public void setTime(String time){this.time=time;}

    public long getId(){return this.id;}

    public long getUid(){return this.uid;}

    public long getTemplateId(){return this.templateId;}

    public int getState(){return this.state;}

    public String getTime(){return this.time;}
}
