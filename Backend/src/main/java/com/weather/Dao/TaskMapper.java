package com.weather.Dao;

import com.weather.PO.ChildrenTaskInstance;
import com.weather.PO.TaskInstance;
import javafx.concurrent.Task;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface TaskMapper {
    List<TaskInstance> getAllTaskInstances();//获取所有用户任务

    List<TaskInstance> getTaskInstanceByUserId(@Param("uid") long uid);//获取用户任务


    List<TaskInstance> searchTaskInstanceByUserId(@Param("searchText")String searchText,@Param("uid")long uid,@Param("state")int state);


    int addTaskInstance(TaskInstance taskInstance);//增加用户任务

    int addChildrenTaskInstance(ChildrenTaskInstance childrenTaskInstance);

    TaskInstance getTaskInstanceById(long id);

    List<ChildrenTaskInstance> getChildrenTaskInstancesByParentId(long parentId);

    List<TaskInstance> getAllTasks();

    int deleteChildrenTaskInstance(long id);

    int deleteTaskInstance(long id);

    List<TaskInstance> getAllTasksOrderedByUser(String searchText);

    List<TaskInstance> getAllTasksOrderedByTime(String searchText);


    List<TaskInstance> selectAllTasksOrderedByUser(@Param("searchText")String searchText,@Param("state") int state);

    List<TaskInstance> selectAllTasksOrderedByTime(@Param("searchText")String searchText,@Param("state")int state);


    int refuseTaskInstance(long id);

    int acceptTaskInstance(long id);

    int runTaskInstance(long id);

    int runChildrenTaskInstance(long id);

    ChildrenTaskInstance getChildrenTaskInstanceById(long id);

    int refuseChildrenTaskInstance(long id);

    int acceptChildrenTaskInstance(long id);

    int setResult(@Param("id") long id,@Param("output") String output);

    int childrenTaskInstanceFinished(long id);

    int childrenTaskInstanceFailed(long id);


    int taskInstanceFinished(long id);

    int taskInstanceFailed(long id);
}
