package com.weather.Dao;

import com.weather.PO.Admin;
import com.weather.PO.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.security.PermitAll;
import java.util.List;

@Mapper
@Service
public interface AccountMapper {
    User getUserByName(@Param("username") String username);

    User getUserById(@Param("id")long id);

    Admin getAdminByName(@Param("username") String username);

    int addUser(User user);

    List<User> searchAllUsers(@Param("searchText") String searchText);

    List<User> getAllUsers();

    List<User> searchAllStateUsers(@Param("searchText") String searchText,@Param("state") int state);

    List<User> getAllStateUsers(@Param("state") int state);

    int changeState(@Param("id") long id,@Param("state") int state);
}
