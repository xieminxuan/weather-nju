package com.weather.Dao;

import com.weather.PO.BaseTaskTemplate;
import com.weather.PO.ChildrenTaskTemplate;
import com.weather.PO.Params;
import com.weather.PO.TaskTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
@Service
public interface TaskTemplateMapper {
    List<TaskTemplate> getTaskTemplates();

    List<ChildrenTaskTemplate> getChildrenTaskTemplates(long parentId);

    List<ChildrenTaskTemplate> getChildrenTaskTemplatesByBaseId(long baseId);

    BaseTaskTemplate getBaseTaskTemplateById(long id);

    List<Params> getParamsByBaseId(long baseId);

    TaskTemplate getTaskTemplateById(long id);

    ChildrenTaskTemplate getChildrenTaskTemplateById(long id);

    int deleteTaskTemplate(long id);

    int updateTaskTemplateByIsDelete(long id);

    int deleteChildrenTemplates(long parentId);

    List<BaseTaskTemplate> getBaseTaskTemplates();

    List<BaseTaskTemplate> selectBaseTaskTemplates(String searchText);

    long addTaskTemplate(TaskTemplate taskTemplate);

    int addChildrenTaskTemplate(ChildrenTaskTemplate childrenTaskTemplate);

    int addBaseTaskTemplate(BaseTaskTemplate baseTaskTemplate);

    int updateBaseTaskTemplateByIsDelete(long id);

    TaskTemplate getTaskTemplateByName(String name);

    int addParam(Params params);

    BaseTaskTemplate getBaseTaskTemplateByName(String name);

    int deleteParamsByBaseId(long baseId);

    int uploadFile(@Param("id") long id,@Param("script") String script);

    int deleteBaseTaskTemplate(long id);

    List<TaskTemplate> selectTaskTemplates(String searchText);

}
