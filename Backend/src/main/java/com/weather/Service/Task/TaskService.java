package com.weather.Service.Task;

import com.weather.VO.ResponseVO;
import com.weather.VO.TaskInstanceForm;


public interface TaskService {
    ResponseVO getTaskInstancesByUserId(long userId,String searchText,String sortType,int state,int current);

    ResponseVO addTaskInstance(TaskInstanceForm taskInstanceForm);

    ResponseVO getTaskInstanceById(long id,int role);

    ResponseVO verifyTask(long id,int verifyType);

    ResponseVO getAllTasks(String searchText,String sortType,int state,int current);

    ResponseVO getFiles(long id);

    ResponseVO executeTask(long id);

    ResponseVO deleteTaskInstanceById(long id);
}
