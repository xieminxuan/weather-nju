package com.weather.Service.Task;

import com.weather.VO.BaseTaskTemplateForm;
import com.weather.VO.ResponseVO;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

public interface TaskTemplateService {
    ResponseVO getTaskTemplates(String searchText,int current);

    ResponseVO getChildrenTaskTemplates(long parentId);

    ResponseVO addTaskTemplate(long id, String name,String description,String baseTasks);

    ResponseVO getBaseTaskTemplates(String searchText,int current);

    ResponseVO deleteTaskTemplate(long id);

    ResponseVO getTaskTemplateInfo(long id);

    ResponseVO addBaseTaskTemplate(BaseTaskTemplateForm baseTaskTemplateForm);

    ResponseVO uploadFile(MultipartFile file,long id);

    ResponseVO deleteBaseTaskTemplate(long id);
}
