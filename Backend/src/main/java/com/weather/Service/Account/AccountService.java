package com.weather.Service.Account;

import com.weather.VO.*;

public interface AccountService {

    UserVO loginUser(UserForm userForm);

    UserVO getUserInfo(long id);

    AdminVO loginAdmin(UserForm userForm);

    ResponseVO addUser(UserForm userForm);

    ResponseVO getAllUsers(String searchText, int state,int current);

    ResponseVO changeState(StateForm stateForm);
}
