package com.weather.Util;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;


/**
 * java执行shell脚本的工具类
 */
public class SSHClient {
    private static final Logger logger=LoggerFactory.getLogger(SSHClient.class);
    private static Session session;
    private static final int TIME_OUT=1000*5*60;
    private static final String Login_Error="登陆错误";

    /**
     * 登陆远端服务器
     */
    public static void login(){
        try {
            String userName = "smart";
            String password = "5m4rtqaz";
            String ip="114.212.48.179";
            JSch jsch = new JSch();
            int port=22;
            session = jsch.getSession(userName, ip, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");//初次登陆提示
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!?????????????????????");
            session.connect(TIME_OUT);
            System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        } catch (JSchException e) {
            logger.info(Login_Error);
            e.printStackTrace();
        }
    }

    /**
     * 执行
     * @param command shell命令脚本
     * @return
     * @throws Exception
     */
    public static String execShell(String command) throws Exception {
        Channel channel=null;
        OutputStream outputStream=null;
        InputStream inputStream=null;
        PrintStream printStream=null;
        BufferedReader bufferedReader=null;
        String result="";
        try{
            channel=session.openChannel("shell");
            outputStream=channel.getOutputStream();
            printStream=new PrintStream(outputStream,true);
            channel.connect();
            printStream.println(command);
            printStream.println("exit");
            printStream.close();
            inputStream=channel.getInputStream();
            bufferedReader=new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line=bufferedReader.readLine())!=null){
                result=result+"\n"+line;
            }
        }catch (JSchException e){
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            channel.disconnect();
        }
        return result;

        /*Channel channel = session.openChannel("exec");
        ChannelShell exec = (ChannelShell) channel;
        // 返回结果流（命令执行错误的信息通过getErrStream获取）
        InputStream stdStream = exec.getInputStream();
        exec.setCommand(command);

        exec.connect();
        try {
            // 开始获得SSH命令的结果
            while (true) {
                while (stdStream.available()>0) {
                    int i = stdStream.read(tmp, 0, 1024);
                    if (i<0){break;}
                    resultBuffer.append(new String(tmp, 0, i));
                }
                if (exec.isClosed()) {break;}
                try {
                    Thread.sleep(200);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } finally {
            channel.disconnect();
        }
        return resultBuffer.toString();*/
    }

    public void getPic(String fileName) throws Exception{
        try{
            //String remotePath="/home/smart/smart-psm-web/resources/"+fileName;
            //String localPath="./pics/"+fileName;
            String remotePath="/home/smart/smart-psm-web/"+fileName;
            String localPath="./"+fileName;
            ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
            channel.connect();
            channel.get(remotePath, localPath, new SftpProgressMonitor() {
                @Override
                public void init(int i, String s, String s1, long l) {
                    System.out.println("download start …");
                }

                @Override
                public boolean count(long l) {
                    System.out.println(l);
                    return false;
                }

                @Override
                public void end() {
                    System.out.println("download complete!");
                }
            });
        }catch (SftpException e){
            e.printStackTrace();
        }
    }
    public void upload(MultipartFile file) throws Exception{
        try{
        ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
        channel.connect();
        channel.cd("/home/smart/smart-psm-web");
        channel.put(file.getInputStream(), file.getOriginalFilename(), new SftpProgressMonitor() {
            @Override
            public void init(int i, String s, String s1, long l) {
                System.out.println("upload start …");
            }

            @Override
            public boolean count(long l) {
                System.out.println(l);
                return false;
            }

            @Override
            public void end() {
                System.out.println("uoload complete!");
            }
        });
    }catch (SftpException e){
            e.printStackTrace();
        }

    }

    /**
     * 关闭连接
     */
    public static void close() {
        if (session.isConnected())
            session.disconnect();
    }

}
