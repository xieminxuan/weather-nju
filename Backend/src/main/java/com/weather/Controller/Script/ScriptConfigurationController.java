package com.weather.Controller.Script;

import com.weather.Service.Task.TaskTemplateService;
import com.weather.VO.BaseTaskTemplateForm;
import com.weather.VO.ResponseVO;
import com.weather.VO.SearchPageForm;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/scriptconfiguration")
public class ScriptConfigurationController {
    @Autowired
    private TaskTemplateService taskTemplateService;

    @RequestMapping(value = "/getBaseTaskTemplates",method = RequestMethod.POST)
    public ResponseVO getBaseTaskTemplates(@RequestBody SearchPageForm searchPageForm){
        return taskTemplateService.getBaseTaskTemplates(searchPageForm.getSearchText(),searchPageForm.getCurrent());
    }

    @RequestMapping(value = "/addBaseTaskTemplate",method = RequestMethod.POST)
    public ResponseVO addBaseTaskTemplate(@RequestBody BaseTaskTemplateForm baseTaskTemplateForm){
        return taskTemplateService.addBaseTaskTemplate(baseTaskTemplateForm);
    }

    @RequestMapping(value = "/uploadFile" ,method = RequestMethod.POST)
    @ResponseBody
    public ResponseVO uploadFile(HttpServletRequest request){
        MultipartHttpServletRequest params=((MultipartHttpServletRequest)request);
        MultipartFile file=((MultipartHttpServletRequest)request).getFile("MultipartFile");
        long id=Long.parseLong(params.getParameter("id"));
        if(file ==null &&  file.isEmpty()){
            return ResponseVO.buildFailure("Empty File.");
        }
        return taskTemplateService.uploadFile(file,id);
    }

    @RequestMapping(value = "/deleteBaseTaskTemplate",method = RequestMethod.DELETE)
    public ResponseVO deleteBaseTaskTemplate(@RequestParam long id){
        return taskTemplateService.deleteBaseTaskTemplate(id);
    }
}
