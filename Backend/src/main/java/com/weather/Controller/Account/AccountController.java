package com.weather.Controller.Account;

import com.weather.Service.Account.AccountService;
import com.weather.VO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class AccountController {

    private final static String ACCOUNT_INFO_ERROR="用户名或密码错误";
    private final static String ACCOUNT_FROZEN="该账户已冻结";

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/loginUser",method = RequestMethod.POST)
    public ResponseVO loginUser(@RequestBody UserForm userForm){
        UserVO user=accountService.loginUser(userForm);
        if(user==null){
            return ResponseVO.buildFailure(ACCOUNT_INFO_ERROR);
        }else if(user.getState()==2){
            return ResponseVO.buildFailure(ACCOUNT_FROZEN);
        }
        return ResponseVO.buildSuccess(user);
    }

    @RequestMapping(value = "/getUserInfo",method = RequestMethod.GET)
    public ResponseVO getUserInfo(@RequestParam("uid") long id){
        UserVO user=accountService.getUserInfo(id);
        if(user==null){
            return ResponseVO.buildFailure(ACCOUNT_INFO_ERROR);
        }
        return ResponseVO.buildSuccess(user);
    }

    @RequestMapping(value = "/loginAdmin",method = RequestMethod.POST)
    public ResponseVO loginAdmin(@RequestBody UserForm userForm){
        AdminVO admin=accountService.loginAdmin(userForm);
        if(admin==null){
            return ResponseVO.buildFailure(ACCOUNT_INFO_ERROR);
        }else if(admin.getState()==2){
            return ResponseVO.buildFailure(ACCOUNT_FROZEN);
        }
        return ResponseVO.buildSuccess(admin);
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public ResponseVO addUser(@RequestBody UserForm userForm){
        return accountService.addUser(userForm);
    }

    @RequestMapping(value = "/getAllUsers",method = RequestMethod.POST)
    public ResponseVO getAllUsers(@RequestBody UserSearchPageForm userSearchPageForm){
        return accountService.getAllUsers(userSearchPageForm.getSearchText(),userSearchPageForm.getState(),userSearchPageForm.getCurrent());
    }

    @RequestMapping(value = "/changeState",method = RequestMethod.POST)
    public ResponseVO changeState(@RequestBody StateForm stateForm){
        return accountService.changeState(stateForm);
    }
}
