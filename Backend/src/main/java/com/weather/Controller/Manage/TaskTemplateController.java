package com.weather.Controller.Manage;

import com.weather.Service.Task.TaskTemplateService;
import com.weather.VO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tasktemplate")
public class TaskTemplateController {
    @Autowired
    private TaskTemplateService taskTemplateService;

    @RequestMapping(value = "/getTaskTemplates",method = RequestMethod.POST)
    public ResponseVO getTaskTemplates(@RequestBody SearchPageForm searchPageForm){
        return taskTemplateService.getTaskTemplates(searchPageForm.getSearchText(),searchPageForm.getCurrent());
    }

    @RequestMapping(value = "/addTaskTemplate",method = RequestMethod.POST)
    public ResponseVO addTaskTemplates(@RequestBody TaskTemplateForm taskTemplateForm){
        return taskTemplateService.addTaskTemplate(taskTemplateForm.getId(), taskTemplateForm.getName(),taskTemplateForm.getDescription(),taskTemplateForm.getBaseTasks());
    }

    @RequestMapping(value = "/getBaseTaskTemplates",method = RequestMethod.POST)
    public ResponseVO getBaseTaskTemplates(@RequestBody SearchPageForm searchPageForm){
        return taskTemplateService.getBaseTaskTemplates(searchPageForm.getSearchText(),searchPageForm.getCurrent());
    }


    @RequestMapping(value = "/deleteTaskTemplate",method = RequestMethod.DELETE)
    public ResponseVO deleteTaskTemplate(@RequestParam("id")long id){
        return taskTemplateService.deleteTaskTemplate(id);
    }

    @RequestMapping(value = "/getTaskTemplateInfo",method = RequestMethod.GET)
    public ResponseVO getTaskTemplateInfo(@RequestParam("id")long id){
        return taskTemplateService.getTaskTemplateInfo(id);
    }
}
