package com.weather.Controller.Workspace;

import com.weather.Service.Task.TaskService;
import com.weather.Service.Task.TaskTemplateService;
import com.weather.VO.ResponseVO;
import com.weather.VO.SearchPageForm;
import com.weather.VO.TaskInstanceForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/userworkspace")
public class UserWorkspaceController {

    @Autowired
    private TaskTemplateService taskTemplateService;
    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/getTaskTemplates",method = RequestMethod.POST)
    public ResponseVO getTaskTemplates(@RequestBody SearchPageForm searchPageForm){
        return taskTemplateService.getTaskTemplates(searchPageForm.getSearchText(),searchPageForm.getCurrent());
    }

    @RequestMapping(value = "/getChildrenTaskTemplates",method = RequestMethod.GET)
    public ResponseVO getChildrenTaskTemplates(@RequestParam("id")long parentId){
        return taskTemplateService.getChildrenTaskTemplates(parentId);
    }

    @RequestMapping(value = "/generateTask",method = RequestMethod.POST)
    public ResponseVO generateTask(@RequestBody TaskInstanceForm taskInstanceForm){
        return taskService.addTaskInstance(taskInstanceForm);
    }

    @RequestMapping(value = "/executeTask",method = RequestMethod.GET)
    public ResponseVO executeTask(@RequestParam("id")long id){
        taskService.verifyTask(id, 0);
        return taskService.executeTask(id);
    }


}
