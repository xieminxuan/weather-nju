package com.weather.Controller.Workspace;

import com.weather.Service.Task.TaskService;
import com.weather.VO.ResponseVO;
import com.weather.VO.SearchTaskForm;
import com.weather.VO.VerifyTaskForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/adminworkspace")
public class AdminWorkspaceController {
    @Autowired
    private TaskService taskService;

    /**
     * 获取所有用户创建过的任务列表
     * searchText 搜索关键字
     * sortType "","user","time","time-reverse"
     * state 任务状态筛选
     * @return
     */
    @RequestMapping(value = "/getAllTasks",method = RequestMethod.POST)
    public ResponseVO getTaskTemplates(@RequestBody SearchTaskForm searchTextForm){
        return taskService.getAllTasks(searchTextForm.getSearchText(),searchTextForm.getSortType(),searchTextForm.getState(),searchTextForm.getCurrent());
    }

    @RequestMapping(value = "/getTaskInfo",method = RequestMethod.GET)
    public ResponseVO getTaskInfo(@RequestParam("id")long id){
        return taskService.getTaskInstanceById(id,1);
    }


    @RequestMapping(value = "/verifyTask",method = RequestMethod.POST)
    public ResponseVO verifyTask(@RequestBody VerifyTaskForm verifyTaskForm){
        return taskService.verifyTask(verifyTaskForm.getId(),verifyTaskForm.getVerifyType());
    }

    @RequestMapping(value = "/executeTask",method = RequestMethod.GET)
    public ResponseVO executeTask(@RequestParam("id")long id){
        /*
        System.out.println("executeTaskController开始");
        taskService.executeTask(id);
        System.out.println("executeTaskController结束");
         */
        return taskService.executeTask(id);

    }
}
