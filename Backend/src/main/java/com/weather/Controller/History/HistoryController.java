package com.weather.Controller.History;

import com.weather.PO.ChildrenTaskInstance;
import com.weather.Service.Task.TaskService;
import com.weather.VO.ResponseVO;
import com.weather.VO.UserTaskForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@RestController
@RequestMapping("/history")
public class HistoryController {
    @Autowired
    private TaskService taskService;

    /**
     * 获取该用户创建过的任务列表
     * searchText 搜索关键字
     * sortType "","time","time-reverse"
     * state 任务状态筛选，state设为3时，代表返回全部
     * @return
     */
    @RequestMapping(value = "/getUserTasks",method = RequestMethod.POST)
    public ResponseVO getUserTasks(@RequestBody UserTaskForm userTaskForm){
        return taskService.getTaskInstancesByUserId(userTaskForm.getUid(),userTaskForm.getSearchText(),userTaskForm.getSortType(),userTaskForm.getState(),userTaskForm.getCurrent());
    }

    @RequestMapping(value = "/getTaskInfo",method = RequestMethod.GET)
    public ResponseVO getTaskInfo(@RequestParam("id")long id){
        return taskService.getTaskInstanceById(id,0);
    }

    @RequestMapping(value = "/deleteTask",method = RequestMethod.DELETE)
    public ResponseVO deleteTask(@RequestParam("id")long id){
        return taskService.deleteTaskInstanceById(id);
    }


    @RequestMapping(value = "/downloadFile")
    @ResponseBody
    public ResponseVO downloadFile(HttpServletResponse response,@RequestParam("name")String name){
        try{
            String fileName = name.substring(name.lastIndexOf('/')+1);
            String filePath="/home/smart/smart-psm-web/"+name;
            File file=new File(filePath);
            //流的形式下载
            InputStream inputStream=new BufferedInputStream(new FileInputStream(filePath));
            byte[] buffer=new byte[inputStream.available()];
            inputStream.read(buffer);
            inputStream.close();
            response.reset();
            response.addHeader("Content-Disposition","attachment;filename="+new String(fileName.getBytes()));//设置弹出框下载
            response.addHeader("Content-Length",""+file.length());
            OutputStream outputStream=new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");//自动匹配文件类型
            outputStream.write(buffer);
            outputStream.flush();
            outputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return ResponseVO.buildSuccess(response);
    }

    /*
        传taskInstance的id
        获取文件名列表
     */
    @RequestMapping(value = "/getFiles",method = RequestMethod.GET)
    public ResponseVO getFiles(@RequestParam("id")long id){
        return taskService.getFiles(id);
    }

}
