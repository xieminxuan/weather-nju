package com.weather.VO;

import com.weather.PO.Admin;

public class AdminVO {
    private long id;
    private String username;
    private String password;
    private int state;

    public AdminVO(Admin admin){
        this.id=admin.getId();
        this.username=admin.getUsername();
        this.password=admin.getPassword();
        this.state=admin.getState();
    }

    public long getId(){return id; }

    public String getUsername(){return username;}

    public String getPassword(){return password;}

    public int getState(){return state;}

    public void setUsername(String username){this.username=username;}

    public void setPassword(String password){this.password=password;}

    public void setState(int state){this.state=state;}
}
