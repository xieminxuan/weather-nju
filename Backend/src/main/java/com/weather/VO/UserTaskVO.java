package com.weather.VO;

import java.util.List;

public class UserTaskVO {
    private long taskInstanceId;
    private String time;
    private String name;
    private long templateId;
    private String templateName;
    private List<String> subnames;
    private int state;

    public UserTaskVO(){}
    public UserTaskVO(long taskInstanceId,String time,String name,long templateId, String templateName,List<String> subnames,int state){
        this.taskInstanceId=taskInstanceId;
        this.time=time;
        this.name=name;
        this.templateId = templateId;
        this.templateName=templateName;
        this.subnames=subnames;
        this.state=state;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubnames(List<String> subnames) {
        this.subnames = subnames;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setTaskInstanceId(long taskInstanceId) {
        this.taskInstanceId = taskInstanceId;
    }

    public String getTime() {
        return time;
    }

    public int getState() {
        return state;
    }

    public String getName() {
        return name;
    }

    public String getTemplateName() {
        return templateName;
    }

    public long getTaskInstanceId() {
        return taskInstanceId;
    }

    public List<String> getSubnames() {
        return subnames;
    }

    public long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }
}
