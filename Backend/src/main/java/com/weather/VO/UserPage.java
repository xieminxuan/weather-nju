package com.weather.VO;

import com.weather.PO.User;

import java.util.List;

public class UserPage {
    private int num;
    private List<User> users;
    public UserPage(){}
    public UserPage(int num,List<User>users){
        this.num=num;
        this.users=users;
    }

    public int getNum() {
        return num;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
