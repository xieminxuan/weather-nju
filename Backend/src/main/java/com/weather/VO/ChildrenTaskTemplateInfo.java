package com.weather.VO;

import com.weather.PO.BaseTaskTemplate;
import com.weather.PO.Params;
import org.apache.ibatis.annotations.Param;
import org.w3c.dom.ls.LSException;
import sun.security.krb5.internal.PAData;

import java.util.ArrayList;
import java.util.List;

public class ChildrenTaskTemplateInfo {
    private long baseId;
    private String name;
    private String description;
    private int index;
    private String script;
    private List<ParamVO> params;

    public ChildrenTaskTemplateInfo(){}
    public ChildrenTaskTemplateInfo(BaseTaskTemplate baseTaskTemplate, List<ParamVO> params, int index){
        this.baseId=baseTaskTemplate.getId();
        this.name=baseTaskTemplate.getName();
        this.description=baseTaskTemplate.getDescription();
        this.index=index;
        this.params=params;
        this.script=baseTaskTemplate.getScriptName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBaseId(long baseId) {
        this.baseId = baseId;
    }

    public void setParams(List<ParamVO> params) {
        this.params = params;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        return index;
    }

    public String getDescription() {
        return description;
    }

    public String getScript() {
        return script;
    }

    public long getBaseId() {
        return baseId;
    }

    public List<ParamVO> getParams() {
        return params;
    }
}
