package com.weather.VO;

import java.util.List;

public class TaskTemplateVOPage {
    private int num;
    private List<TaskTemplateVO> resList;

    public TaskTemplateVOPage(){}
    public TaskTemplateVOPage(int num,List<TaskTemplateVO>resList){
        this.num=num;
        this.resList=resList;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setResList(List<TaskTemplateVO> resList) {
        this.resList = resList;
    }

    public int getNum() {
        return num;
    }

    public List<TaskTemplateVO> getResList() {
        return resList;
    }
}
