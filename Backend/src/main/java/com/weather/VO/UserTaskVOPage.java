package com.weather.VO;

import java.util.List;

public class UserTaskVOPage {
    private int num;
    private List<UserTaskVO> resList;
    public UserTaskVOPage(){}
    public UserTaskVOPage(int num,List<UserTaskVO> resList){
        this.num=num;
        this.resList=resList;
    }

    public int getNum() {
        return num;
    }

    public List<UserTaskVO> getResList() {
        return resList;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setResList(List<UserTaskVO> resList) {
        this.resList= resList;
    }
}
