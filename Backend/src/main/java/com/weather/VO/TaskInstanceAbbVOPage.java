package com.weather.VO;

import java.util.List;

public class TaskInstanceAbbVOPage {
    private int num;
    private List<TaskInstanceAbbVO> resList;
    public TaskInstanceAbbVOPage(){}
    public TaskInstanceAbbVOPage(int num,List<TaskInstanceAbbVO> resList){
        this.num=num;
        this.resList=resList;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setResList(List<TaskInstanceAbbVO> resList) {
        this.resList = resList;
    }

    public int getNum() {
        return num;
    }

    public List<TaskInstanceAbbVO> getResList() {
        return resList;
    }
}
