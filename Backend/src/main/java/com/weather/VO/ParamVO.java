package com.weather.VO;

import com.weather.PO.Params;
import org.apache.ibatis.annotations.Param;

public class ParamVO {
    private String name;
    private String cname; //中文名
    private String type;
    private String values;

    public ParamVO(){}
    public ParamVO(Params params){
        this.name=params.getName();
        this.type=params.getType();
        this.values=params.getValue();
        this.cname=params.getCname();
    }
    public ParamVO(String name,String cname,String type,String values){
        this.name=name;
        this.cname=cname;
        this.type=type;
        this.values=values;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public String getName() {
        return name;
    }

    public String getCname() {
        return cname;
    }

    public String getType() {
        return type;
    }

    public String getValues() {
        return values;
    }
}
