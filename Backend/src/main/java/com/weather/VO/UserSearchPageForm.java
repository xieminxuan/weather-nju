package com.weather.VO;

public class UserSearchPageForm {
    private String searchText;
    private int state;
    private int current;
    public UserSearchPageForm(){}
    public UserSearchPageForm(String searchText,int state,int current){
        this.searchText=searchText;
        this.state=state;
        this.current=current;
    }

    public int getCurrent() {
        return current;
    }

    public String getSearchText() {
        return searchText;
    }

    public int getState() {
        return state;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setState(int state) {
        this.state = state;
    }
}
