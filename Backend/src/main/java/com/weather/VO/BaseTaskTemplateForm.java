package com.weather.VO;

import java.util.List;


public class BaseTaskTemplateForm {
    private long id;
    private String name;
    private String description;
    private List<ParamVO> params;
    private String scriptName;
    private String outLogName; //日志文件名称

    public BaseTaskTemplateForm(){}

    public BaseTaskTemplateForm(long id, String name, String description, List<ParamVO> params, String scriptName, String outLogName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.params = params;
        this.scriptName = scriptName;
        this.outLogName = outLogName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ParamVO> getParams() {
        return params;
    }

    public void setParams(List<ParamVO> params) {
        this.params = params;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getOutLogName() {
        return outLogName;
    }

    public void setOutLogName(String outLogName) {
        this.outLogName = outLogName;
    }
}
