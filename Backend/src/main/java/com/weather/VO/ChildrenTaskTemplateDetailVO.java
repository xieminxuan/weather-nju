package com.weather.VO;

import com.weather.PO.BaseTaskTemplate;
import com.weather.PO.ChildrenTaskTemplate;
import com.weather.PO.Params;

import java.util.List;

public class ChildrenTaskTemplateDetailVO {
    private long id;
    private String name;
    private String description;
    private int index;
    private String script;
    private List<ParamVO> params;

    public ChildrenTaskTemplateDetailVO(){}
    public ChildrenTaskTemplateDetailVO(long id,String name,String description,int index,String script,List<ParamVO> params){
        this.id=id;
        this.name=name;
        this.description=description;
        this.index=index;
        this.script=script;
        this.params=params;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setParams(List<ParamVO> params) {
        this.params=params;
    }

    public String getName() {
        return name;
    }

    public String getScript() {
        return script;
    }

    public String getDescription() {
        return description;
    }

    public long getId() {
        return id;
    }

    public int getIndex() {
        return index;
    }

    public List<ParamVO> getParams() {
        return params;
    }
}
