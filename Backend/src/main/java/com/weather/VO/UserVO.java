package com.weather.VO;

import com.weather.PO.User;

public class UserVO {
    private long id;
    private String username;
    private String password;
    private int state; //状态 正常为1， 冻结为2

    public UserVO(User user){
        this.id=user.getId();
        this.username=user.getUsername();
        this.password=user.getPassword();
        this.state=user.getState();
    }

    public long getId(){return id; }

    public String getUsername(){return username;}

    public String getPassword(){return password;}

    public int getState(){return state;}

    public void setUsername(String username){this.username=username;}

    public void setPassword(String password){this.password=password;}

    public void setState(int state){this.state=state;}
}
