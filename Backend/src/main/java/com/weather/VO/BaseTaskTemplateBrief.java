package com.weather.VO;

import com.weather.PO.BaseTaskTemplate;

import java.util.List;

public class BaseTaskTemplateBrief {
    private long id;
    private String name;
    private String description;
    private String scriptName;
    private List<ParamVO> params;
    private String outLogName;



    public BaseTaskTemplateBrief(){}

    public BaseTaskTemplateBrief(long id, String name, String description, String scriptName, List<ParamVO> params, String outLogName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.scriptName = scriptName;
        this.params = params;
        this.outLogName = outLogName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public List<ParamVO> getParams() {
        return params;
    }

    public void setParams(List<ParamVO> params) {
        this.params = params;
    }

    public String getOutLogName() {
        return outLogName;
    }

    public void setOutLogName(String outLogName) {
        this.outLogName = outLogName;
    }
}
