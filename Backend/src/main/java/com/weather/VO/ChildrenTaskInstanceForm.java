package com.weather.VO;

public class ChildrenTaskInstanceForm {
    private long uid;
    private long parentId;
    private long childrenTemplateId;
    private String input;
    private int state;

    public ChildrenTaskInstanceForm(){}
    public ChildrenTaskInstanceForm(long uid,long parentId,long childrenTemplateId,String input,int state){
        this.uid=uid;
        this.parentId=parentId;
        this.childrenTemplateId=childrenTemplateId;
        this.input=input;
        this.state=state;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void setChildrenTemplateId(long childrenTemplateId) {
        this.childrenTemplateId = childrenTemplateId;
    }

    public long getParentId() {
        return parentId;
    }

    public String getInput() {
        return input;
    }

    public long getChildrenTemplateId() {
        return childrenTemplateId;
    }

    public int getState() {
        return state;
    }

    public long getUid() {
        return uid;
    }
}
