package com.weather.VO;

public class VerifyTaskForm {
    private long id;
    private int verifyType;
    public VerifyTaskForm(){}
    public VerifyTaskForm(long id,int verifyType){
        this.id=id;
        this.verifyType=verifyType;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setVerifyType(int verifyType) {
        this.verifyType = verifyType;
    }

    public long getId() {
        return id;
    }

    public int getVerifyType() {
        return verifyType;
    }
}
