package com.weather.VO;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public class TaskInstanceDetailVO {
    private long id;
    private String username; //创建该任务的用户
    private String time;
    private String name;
    private int state;
    private List<ChildrenTaskInfoDetail> childrenTaskInfo;

    public TaskInstanceDetailVO(long id, String username, String time, String name, int state, List<ChildrenTaskInfoDetail> childrenTaskInfo) {
        this.id = id;
        this.username = username;
        this.time = time;
        this.name = name;
        this.state = state;
        this.childrenTaskInfo = childrenTaskInfo;
    }



    public TaskInstanceDetailVO(){}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<ChildrenTaskInfoDetail> getChildrenTaskInfo() {
        return childrenTaskInfo;
    }

    public void setChildrenTaskInfo(List<ChildrenTaskInfoDetail> childrenTaskInfo) {
        this.childrenTaskInfo = childrenTaskInfo;
    }
}

