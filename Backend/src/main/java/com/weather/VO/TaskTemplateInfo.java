package com.weather.VO;

import com.weather.PO.TaskTemplate;
import javafx.concurrent.Task;

import java.util.List;

public class TaskTemplateInfo {
    private long id;
    private String name;
    private String description;
    private List<ChildrenTaskTemplateInfo> childrenTaskTemplates;

    public TaskTemplateInfo(){}
    public TaskTemplateInfo(TaskTemplate taskTemplate,List<ChildrenTaskTemplateInfo> childrenTaskTemplates){
        this.id=taskTemplate.getId();
        this.name=taskTemplate.getName();
        this.description=taskTemplate.getDescription();
        this.childrenTaskTemplates=childrenTaskTemplates;
}

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public List<ChildrenTaskTemplateInfo> getChildrenTaskTemplates() {
        return childrenTaskTemplates;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setChildrenTaskTemplates(List<ChildrenTaskTemplateInfo> childrenTaskTemplates) {
        this.childrenTaskTemplates = childrenTaskTemplates;
    }
}
