package com.weather.VO;

public class StateForm {
    private long id;
    private int state;
    public StateForm(){}
    public StateForm(long id,int state){
        this.id=id;
        this.state=state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public long getId() {
        return id;
    }
}
