package com.weather.VO;

public class TaskParamVO {
    private String name;
    private String cname;
    private String value;

    public TaskParamVO(){}
    public TaskParamVO(String name,String cname,String value){
        this.name=name;
        this.cname=cname;
        this.value=value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCname() {
        return cname;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getValue() {
        return value;
    }
}
