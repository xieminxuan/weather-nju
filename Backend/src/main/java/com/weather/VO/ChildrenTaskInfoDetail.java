package com.weather.VO;

import java.util.List;

public class ChildrenTaskInfoDetail {
    private String templateName;
    private int state;
    private List<TaskParamVO> params;

    public ChildrenTaskInfoDetail(String templateName, int state, List<TaskParamVO> params) {
        this.templateName = templateName;
        this.state = state;
        this.params = params;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public List<TaskParamVO> getParams() {
        return params;
    }

    public void setParams(List<TaskParamVO> params) {
        this.params = params;
    }
}
