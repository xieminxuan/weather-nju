package com.weather.VO;

public class SearchPageForm {
    private String searchText;
    private int current;

    public SearchPageForm(){}
    public SearchPageForm(String searchText,int current){
        this.searchText=searchText;
        this.current=current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public int getCurrent() {
        return current;
    }

    public String getSearchText() {
        return searchText;
    }
}
