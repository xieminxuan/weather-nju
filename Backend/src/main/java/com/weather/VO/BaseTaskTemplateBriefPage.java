package com.weather.VO;

import java.util.List;

public class BaseTaskTemplateBriefPage {
    private int num;
    List<BaseTaskTemplateBrief> resList;

    public BaseTaskTemplateBriefPage(){}
    public BaseTaskTemplateBriefPage(int num,List<BaseTaskTemplateBrief> resList){
        this.num=num;
        this.resList=resList;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setResList(List<BaseTaskTemplateBrief> resList) {
        this.resList = resList;
    }

    public int getNum() {
        return num;
    }

    public List<BaseTaskTemplateBrief> getResList() {
        return resList;
    }
}
