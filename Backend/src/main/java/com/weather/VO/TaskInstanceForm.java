package com.weather.VO;


public class TaskInstanceForm {
    private long uid;// 发起请求的用户id
    private String name;
    private long templateId; //基于哪个任务模版的id
    private String time;//任务创建的时间
    private String input;

    public TaskInstanceForm(){}
    public TaskInstanceForm(long uid,String name,long templateId,int state,String time,String input){
        this.uid=uid;
        this.name=name;
        this.templateId=templateId;
        this.time=time;
        this.input=input;
    }

    public void setUid(long uid){this.uid=uid;}

    public void setTemplateId(long templateId){this.templateId=templateId;}

    public void setName(String name) {
        this.name = name;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void setTime(String time){this.time=time;}

    public long getUid(){return this.uid;}

    public long getTemplateId(){return this.templateId;}

    public String getName() {
        return name;
    }

    public String getTime(){return this.time;}

    public String getInput() {
        return input;
    }
}
