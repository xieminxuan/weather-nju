package com.weather.VO;

public class UserTaskForm {
    private long uid;
    private String searchText;
    private int state;
    private String sortType;
    private int current;


    public UserTaskForm(){}
    public UserTaskForm(long uid,String searchText,int state,String sortType,int current){
        this.uid=uid;
        this.searchText=searchText;
        this.state=state;
        this.sortType=sortType;
        this.current=current;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getSortType() {
        return sortType;
    }

    public String getSearchText() {
        return searchText;
    }

    public int getState() {
        return state;
    }

    public long getUid() {
        return uid;
    }
}
