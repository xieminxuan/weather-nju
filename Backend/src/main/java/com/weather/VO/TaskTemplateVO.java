package com.weather.VO;

import com.weather.PO.TaskTemplate;

public class TaskTemplateVO {
    private long id;
    private String name;
    private String description;

    public TaskTemplateVO(){}
    public TaskTemplateVO(TaskTemplate taskTemplate){
        this.id=taskTemplate.getId();
        this.name=taskTemplate.getName();
        this.description= taskTemplate.getDescription();
    }
    public void setId(long id){this.id=id;}

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description){
        this.description=description;
    }

    public long getId(){return id;}

    public String getName(){return name;}

    public String getDescription(){return description;}
}
