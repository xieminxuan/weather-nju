package com.weather.VO;

public class SearchTaskForm {
    private String searchText;
    private String sortType;
    private int state;
    private int current;
    public SearchTaskForm(){}
    public SearchTaskForm(String searchText,String sortType,int state,int current) {
        this.searchText=searchText;
        this.sortType=sortType;
        this.state=state;
        this.current=current;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public int getState() {
        return state;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public int getCurrent() {
        return current;
    }

    public String getSearchText() {
        return searchText;
    }

    public String getSortType() {
        return sortType;
    }
}
