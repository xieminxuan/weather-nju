package com.weather.VO;

import com.weather.PO.ChildrenTaskTemplate;

public class ChildrenTaskTemplateVO {
    private long id;
    private long parentId;// 所属的任务模版
    private long baseId;
    private int index; // 子任务所属于的节点 第一个 第二个 第三个

    public ChildrenTaskTemplateVO(){}

    public ChildrenTaskTemplateVO(ChildrenTaskTemplate childrenTaskTemplate){
        this.id=childrenTaskTemplate.getId();
        this.parentId=childrenTaskTemplate.getParentId();
        this.baseId=childrenTaskTemplate.getBaseId();
        this.index=childrenTaskTemplate.getIndex();
    }

    public void setId(long id){this.id=id;}

    public void setParentId(long parentId){this.parentId=parentId;}

    public void setBaseId(long baseId) {
        this.baseId = baseId;
    }

    public void setIndex(int index){this.index=index;}

    public long getId(){return this.id;}

    public long getParentId(){return this.parentId;}

    public long getBaseId() {
        return baseId;
    }

    public int getIndex(){return this.index;}


}
