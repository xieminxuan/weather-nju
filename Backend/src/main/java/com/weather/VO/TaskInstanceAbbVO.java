package com.weather.VO;

import com.weather.PO.TaskInstance;

import java.util.List;

public class TaskInstanceAbbVO {
    private long id;
    private long uid;
    private String username;
    private String time;
    private String name;
    private long templateId;
    private String templateName;
    private List<String> subnames;
    private int state;

    public TaskInstanceAbbVO(){}

    public TaskInstanceAbbVO(TaskInstance taskInstance,String username, String templateName,List<String> subnames){
        this.id=taskInstance.getId();
        this.uid=taskInstance.getUid();
        this.username=username;
        this.time=taskInstance.getTime();
        this.name=taskInstance.getName();
        this.templateId = taskInstance.getTemplateId();
        this.templateName=templateName;
        this.subnames=subnames;
        this.state=taskInstance.getState();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setSubnames(List<String> subnames) {
        this.subnames = subnames;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getName() {
        return name;
    }

    public long getUid() {
        return uid;
    }

    public int getState() {
        return state;
    }

    public long getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getUsername() {
        return username;
    }

    public List<String> getSubnames() {
        return subnames;
    }

    public long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(long templateId) {
        this.templateId = templateId;
    }
}
