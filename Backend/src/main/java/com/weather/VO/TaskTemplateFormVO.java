package com.weather.VO;

public class TaskTemplateFormVO {
    private long id;
    private String name;
    private String description;
    private String baseTasks;

    public TaskTemplateFormVO(){}
    public TaskTemplateFormVO(long id,String name,String description,String baseTasks){
        this.id=id;
        this.name=name;
        this.description=description;
        this.baseTasks=baseTasks;
    }

    public void setBaseTasks(String baseTasks) {
        this.baseTasks = baseTasks;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public String getBaseTasks() {
        return baseTasks;
    }
}
