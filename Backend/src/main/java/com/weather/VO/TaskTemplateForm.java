package com.weather.VO;

public class TaskTemplateForm {
    private long id;
    private String name;
    private String description;
    private String baseTasks;

    public TaskTemplateForm(){}
    public TaskTemplateForm(long id,String name,String description,String baseTasks){
        this.id = id;
        this.name=name;
        this.description=description;
        this.baseTasks=baseTasks;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBaseTasks(String baseTasks) {
        this.baseTasks = baseTasks;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBaseTasks() {
        return baseTasks;
    }
}
