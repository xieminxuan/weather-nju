package com.weather.VO;

public class FileAbbr {
    long id;//子任务模板id
    String name;//子任务模板名字
    String fileName;//文件名
    String picBase64;//图片

    public FileAbbr(){}
    public  FileAbbr(long id,String name,String fileName,String picBase64){
        this.id=id;
        this.name=name;
        this.fileName=fileName;
        this.picBase64=picBase64;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPicBase64() {
        return picBase64;
    }

    public void setPicBase64(String picBase64) {
        this.picBase64 = picBase64;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
